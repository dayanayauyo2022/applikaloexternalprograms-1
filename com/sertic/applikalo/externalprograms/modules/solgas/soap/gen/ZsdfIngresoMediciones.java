
package com.sertic.applikalo.externalprograms.modules.solgas.soap.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItIngresoMedicion" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZsdttIngmedicion"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itIngresoMedicion"
})
@XmlRootElement(name = "ZsdfIngresoMediciones")
public class ZsdfIngresoMediciones {

    @XmlElement(name = "ItIngresoMedicion", required = true)
    protected ZsdttIngmedicion itIngresoMedicion;

    /**
     * Obtiene el valor de la propiedad itIngresoMedicion.
     * 
     * @return
     *     possible object is
     *     {@link ZsdttIngmedicion }
     *     
     */
    public ZsdttIngmedicion getItIngresoMedicion() {
        return itIngresoMedicion;
    }

    /**
     * Define el valor de la propiedad itIngresoMedicion.
     * 
     * @param value
     *     allowed object is
     *     {@link ZsdttIngmedicion }
     *     
     */
    public void setItIngresoMedicion(ZsdttIngmedicion value) {
        this.itIngresoMedicion = value;
    }

}

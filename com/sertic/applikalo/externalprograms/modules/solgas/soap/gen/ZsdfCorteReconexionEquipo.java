
package com.sertic.applikalo.externalprograms.modules.solgas.soap.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItCorteReconexEquipo" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZsdttCorecequipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itCorteReconexEquipo"
})
@XmlRootElement(name = "ZsdfCorteReconexionEquipo")
public class ZsdfCorteReconexionEquipo {

    @XmlElement(name = "ItCorteReconexEquipo", required = true)
    protected ZsdttCorecequipo itCorteReconexEquipo;

    /**
     * Obtiene el valor de la propiedad itCorteReconexEquipo.
     * 
     * @return
     *     possible object is
     *     {@link ZsdttCorecequipo }
     *     
     */
    public ZsdttCorecequipo getItCorteReconexEquipo() {
        return itCorteReconexEquipo;
    }

    /**
     * Define el valor de la propiedad itCorteReconexEquipo.
     * 
     * @param value
     *     allowed object is
     *     {@link ZsdttCorecequipo }
     *     
     */
    public void setItCorteReconexEquipo(ZsdttCorecequipo value) {
        this.itCorteReconexEquipo = value;
    }

}

# PROGRAMAS EXTERNOS #

Contenedor de programas externos.

Todos los programas externos son servicios propiamente de este proyecto (PROGRAMASEXTERNOS) que son llamados desde 
el proyecto EMAREPORTES y cuentan con un body y unos headers establecidos. En los headers viaja un token el cual 
tiene la siguiente informaci�n:
- llave secreta: S3R4TIC_4PIR34D
- objeto json:
    ```
        {
          "idPublico": "3fbeb813ec52f0f0ea1a92e6b33022367cf67ad4a9bc780e6ac50c608f90369e",
          "versionAplicacion": 795,
          "prefijo": "desa",
          "urlApiRead": "https://ema2readerdesa.latinapps.co/apiread/",
          "urlApiWrite": "https://ema2writerdesa.latinapps.co/apiwrite/"
        }
    ```

## PROGRAMAS EXTERNOS ##

* [Conteo de actividades](src/main/java/com/seratic/programasexternos/modules/conteoactividades/DOC_CONTEO.md)
* [Conteo de visitas](src/main/java/com/seratic/programasexternos/modules/conteovisitas/DOC_CONTEO_VISITAS.md)
* [Calculo de riesgos](src/main/java/com/seratic/programasexternos/modules/calculoriesgos/DOC_RIESGOS.md)
* [Crear/Actualizar objetos](src/main/java/com/seratic/programasexternos/modules/crearactualizarobjetos/DOC_OBJETOS.md)
    - [A partir de Valor Est�tico](src/main/java/com/seratic/programasexternos/modules/crearactualizarobjetos/DOC_OBJETOS.md)
    - [A partir de Valor WF](src/main/java/com/seratic/programasexternos/modules/crearactualizarobjetos/DOC_OBJETOS.md)
    - [A partir de Rango de estados](src/main/java/com/seratic/programasexternos/modules/crearactualizarobjetos/DOC_OBJETOS.md)
    - [A partir de Funciones JS](src/main/java/com/seratic/programasexternos/modules/crearactualizarobjetos/DOC_OBJETOS.md)
* [Amauta](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md)
    - [Asignar Lecciones](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/asignarLecciones)
    - [Asignar feedback](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/asignarEvaluacionTareas(feedback))
    - [Calcular avance lecciones](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/calcularAvanceCurso)
    - [Calcular avance feedback](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/calcularAvanceCursoFeedback)
    - [Calcular avance respuesta feedback](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/calcularAvanceRespuestaFeedback)
    - [Actualizar Estado Wf estudianteLeccion](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/ActualizarEstadoWFEstudianteLeccion)
* [Solgas](src/main/java/com/seratic/programasexternos/modules/solgas/SOLGAS.md)
    - [Calcular Promedio](src/main/java/com/seratic/programasexternos/modules/solgas/SOLGAS.md#/solgas/calcularPromedio)
* [Activos Mineros](src/main/java/com/seratic/programasexternos/modules/activosmineros/ActivosMineros)
    - [Notificacion Proyectos Fuera de Plazo](src/main/java/com/seratic/programasexternos/modules/activosmineros/ActivosMineros#/activosmineros/notificar)
    
* Generacion Token [ejempl0](https://drive.google.com/file/d/1jy1oUbdhRyrWb0LV4sxle8gi6uUgBK3v/view?usp=sharing):

    ```
        {
          "idPublico": "d83b3afb2b5787958bc1430d7f274e56af1caace44b989656345a748da63a1a3",
          "versionAplicacion": 583,
          "prefijo": "desa",
          "urlApiRead": "https://ema2readerdesa.latinapps.co/apiread/",
          "exp": 1591932052,
          "urlApiWrite": "http://localhost:8088/"
        }
    ```
clave : S3R4TIC_4PIR34D 


## CRON (tareas programadas) ##

* [Cron Notificar Proyectos fuera de plazo](src/main/java/com/seratic/programasexternos/modules/activosmineros/ActivosMineros)
* [Cron Notificar Proyectos campos vacios](src/main/java/com/seratic/programasexternos/modules/activosmineros/ActivosMineros)  
* [Cron Envio Correo leccion en vivo](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/task/EnvioCorreoTask)
* [Cron Reiniciar Contadores](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/notificarCorreoEstudianteLeccionCron)
* [Cron Ejecutar programas externos erroneos](src/main/java/com/seratic/programasexternos/modules/integracion/DOC_INTEGRACION.md)
* [Cron Asignar feedback al admin](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/asignarFeedAdminCron)
* [Cron Notificar Estudiantes conferencias](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/notificarCorreoEstudianteLeccionCron)
* [Cron Aprobar feedback_estudiante](src/main/java/com/seratic/programasexternos/modules/amauta/AMAUTA.md#/amauta/aprobarFeedAdminCron)
package com.seratic.applikalo.externalprograms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.seratic.applikalo.externalprograms.modules.amauta.utils.CrearBodyUtils;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class CrearBodyUtilsTest {
    
    @Test
	void contextLoads() {
	}

	@Test
	public void testBetween() {
		List<String> condiciones = new ArrayList<>();
		condiciones.add("1");
		condiciones.add("2");
		String result = CrearBodyUtils.obtnerCondicionBetween(condiciones);
		assertEquals("'1' AND '2'",result);
	} 

    @Test
	public void testIn() {
		List<String> condiciones = new ArrayList<>();
		condiciones.add("1");
		condiciones.add("2");
		String result = CrearBodyUtils.obtnerCondicionIn(condiciones);
		assertEquals("'1' ,'2'",result);
	} 

    @Test
	public void testObtenerCampo() {
		String campo = "estudiante";
		JsonNode result = CrearBodyUtils.obtenerCampo(campo);
		assertEquals("{\"campo\":\"estudiante\"}",result.toString());
	} 

    @Test
	public void testObtenerFiltro() {
		JsonNode result = CrearBodyUtils.obtenerFiltro("id", "=", "AND", "estudiante", "2");
		assertEquals("{\"campo\":\"id\",\"condicional\":\"=\",\"conjuncion\":\"AND\",\"objeto\":\"estudiante\",\"parametro\":\"2\"}",result.toString());
	}
}

package com.seratic.applikalo.externalprograms.modules.integracion.mongodb.service;

import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity.ObjetoConteo;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.repository.ObjetoConteoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ObjetoConteoService {
    @Autowired
    ObjetoConteoRepository objetoConteoRepository;

    public ObjetoConteo saveUpdateObjetoConteo(ObjetoConteo objetoConteo) {
        return objetoConteoRepository.save(objetoConteo);
    }

    public boolean deleteObjetoConteo(ObjetoConteo objetoConteo) {
        try {
            objetoConteoRepository.delete(objetoConteo);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ObjetoConteo getObjetoConteoById(String id) {
        return objetoConteoRepository.findById(id).orElse(null);
    }

    public List<ObjetoConteo> getAllObjetoConteo() {
        return objetoConteoRepository.findAll();
    }
}

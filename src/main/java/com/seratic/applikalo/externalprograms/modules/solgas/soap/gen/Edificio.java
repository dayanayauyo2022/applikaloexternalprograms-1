//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.23 a las 12:16:31 PM COT 
//
package com.seratic.applikalo.externalprograms.modules.solgas.soap.gen;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para edificio complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="edificio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="envionotificacion" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="correonotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="activoedificio" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="distrito" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}distrito" minOccurs="0"/>
 *         &lt;element name="provincia" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}provincia" minOccurs="0"/>
 *         &lt;element name="departamento" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}departamento" minOccurs="0"/>
 *         &lt;element name="ubicacion" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}ubicacion" minOccurs="0"/>
 *         &lt;element name="direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "edificio", propOrder = {
    "id",
    "codigo",
    "envionotificacion",
    "correonotificacion",
    "activoedificio",
    "tipo",
    "distrito",
    "provincia",
    "departamento",
    "ubicacion",
    "direccion",
    "nombre"
})
@JsonTypeName(value = "edificio")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Edificio {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Integer id;
    protected String codigo;
    protected Boolean envionotificacion;
    protected String correonotificacion;
    protected Boolean activoedificio;
    protected String tipo;
    protected Distrito distrito;
    protected Provincia provincia;
    protected Departamento departamento;
    protected Ubicacion ubicacion;
    protected String direccion;
    protected String nombre;

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad codigo.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad envionotificacion.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isEnvionotificacion() {
        return envionotificacion;
    }

    /**
     * Define el valor de la propiedad envionotificacion.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setEnvionotificacion(Boolean value) {
        this.envionotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad correonotificacion.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCorreonotificacion() {
        return correonotificacion;
    }

    /**
     * Define el valor de la propiedad correonotificacion.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCorreonotificacion(String value) {
        this.correonotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad activoedificio.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isActivoedificio() {
        return activoedificio;
    }

    /**
     * Define el valor de la propiedad activoedificio.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setActivoedificio(Boolean value) {
        this.activoedificio = value;
    }

    /**
     * Obtiene el valor de la propiedad tipo.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Define el valor de la propiedad tipo.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Obtiene el valor de la propiedad distrito.
     *
     * @return possible object is {@link Distrito }
     *
     */
    public Distrito getDistrito() {
        return distrito;
    }

    /**
     * Define el valor de la propiedad distrito.
     *
     * @param value allowed object is {@link Distrito }
     *
     */
    public void setDistrito(Distrito value) {
        this.distrito = value;
    }

    /**
     * Obtiene el valor de la propiedad provincia.
     *
     * @return possible object is {@link Provincia }
     *
     */
    public Provincia getProvincia() {
        return provincia;
    }

    /**
     * Define el valor de la propiedad provincia.
     *
     * @param value allowed object is {@link Provincia }
     *
     */
    public void setProvincia(Provincia value) {
        this.provincia = value;
    }

    /**
     * Obtiene el valor de la propiedad departamento.
     *
     * @return possible object is {@link Departamento }
     *
     */
    public Departamento getDepartamento() {
        return departamento;
    }

    /**
     * Define el valor de la propiedad departamento.
     *
     * @param value allowed object is {@link Departamento }
     *
     */
    public void setDepartamento(Departamento value) {
        this.departamento = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacion.
     *
     * @return possible object is {@link Ubicacion }
     *
     */
    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    /**
     * Define el valor de la propiedad ubicacion.
     *
     * @param value allowed object is {@link Ubicacion }
     *
     */
    public void setUbicacion(Ubicacion value) {
        this.ubicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad direccion.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

}

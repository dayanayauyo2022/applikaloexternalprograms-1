package com.seratic.applikalo.externalprograms.modules.integracion.mongodb.repository;

import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity.ObjetoConteo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObjetoConteoRepository extends MongoRepository<ObjetoConteo, String> {
}

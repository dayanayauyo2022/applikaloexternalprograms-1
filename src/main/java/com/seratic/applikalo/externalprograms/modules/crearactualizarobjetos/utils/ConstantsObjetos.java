package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils;

public class ConstantsObjetos {

    public static final String TIPO_CAMPO_OBJETO = "5";
    public static final String TIPO_CAMPO_RELACION = "10";
    public static final String TIPO_CAMPO_FECHA = "3";

    public static final String TIPO_CONFIG_WF = "1";
    public static final String TIPO_CONFIG_FUNCION = "2";
    public static final String TIPO_CONFIG_ESTATICO = "3";
    public static final String TIPO_CONFIG_RANGO_ESTADO = "4";

    public static final String VALOR_FECHA_ACTUAL = "NOW";

}

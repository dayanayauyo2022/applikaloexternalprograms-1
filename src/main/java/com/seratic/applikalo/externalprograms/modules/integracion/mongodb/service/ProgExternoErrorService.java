package com.seratic.applikalo.externalprograms.modules.integracion.mongodb.service;

import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyError;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity.ProgExternoError;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.repository.ProgExternoErrorRepository;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.conteoactividades.utils.ConteoActividadesUtils.generarPkObjetoMongo;

@Service
public class ProgExternoErrorService {
    @Autowired
    ProgExternoErrorRepository progExternoErrorRepository;

    public ProgExternoError saveUpdateProgExternoError(ProgExternoError progExternoError) {
        try {
            return progExternoErrorRepository.save(progExternoError);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean deleteProgExternoError(ProgExternoError progExternoError) {
        try {
            progExternoErrorRepository.delete(progExternoError);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ProgExternoError getProgExternoErrorById(String id) {
        return progExternoErrorRepository.findById(id).orElse(null);
    }

    public List<ProgExternoError> getAllProgExternoError() {
        return progExternoErrorRepository.findAll();
    }

    public boolean findAndDeleteProgExternoError(String key, String prefijo, Integer version, String nombreObjeto, String idObjeto) {
        try {
            String idProgExternoError = generarPkObjetoMongo(key, prefijo, version, nombreObjeto, idObjeto);
            ProgExternoError progExternoError = getProgExternoErrorById(idProgExternoError);
            if (progExternoError != null) {
                progExternoErrorRepository.delete(progExternoError);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean findAndSaveUpdateProgExternoError(String idProgExternoError, String url, String tipoPeticion, String body, DataToken dataToken, List<MyError> erroresLst) {
        try {
            ProgExternoError progExternoError = getProgExternoErrorById(idProgExternoError);
            if (progExternoError != null) {
                // actualizar objeto
                Integer numEjecuciones  = 0;
                if (progExternoError.getNumeroEjecuciones() != null) {
                    numEjecuciones = progExternoError.getNumeroEjecuciones();
                }
                progExternoError.setBody(body);
                progExternoError.setTipoPeticion(tipoPeticion);
                progExternoError.setNumeroEjecuciones(numEjecuciones + 1);
                progExternoError.setIdPublicoUsuario(dataToken.getIdPublico());
                progExternoError.setUrlApiRead(dataToken.getUrlApiRead());
                progExternoError.setUrlApiWrite(dataToken.getUrlApiWrite());
                progExternoError.setErroresLst(erroresLst);
                progExternoError.setFecha(new Date());
                progExternoErrorRepository.save(progExternoError);
            } else {
                // crear nuevo:
                ProgExternoError progExternoErrorNew = new ProgExternoError();
                progExternoErrorNew.setId(idProgExternoError);
                progExternoErrorNew.setUrl(url);
                progExternoErrorNew.setTipoPeticion(tipoPeticion);
                progExternoErrorNew.setBody(body);
                progExternoErrorNew.setNumeroEjecuciones(1);
                progExternoErrorNew.setPrefijo(dataToken.getPrefijo());
                progExternoErrorNew.setVersionApp(dataToken.getVersionAplicacion());
                progExternoErrorNew.setUrlApiRead(dataToken.getUrlApiRead());
                progExternoErrorNew.setUrlApiWrite(dataToken.getUrlApiWrite());
                progExternoErrorNew.setIdPublicoUsuario(dataToken.getIdPublico());
                progExternoErrorNew.setErroresLst(erroresLst);
                progExternoErrorNew.setFecha(new Date());
                progExternoErrorRepository.save(progExternoErrorNew);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

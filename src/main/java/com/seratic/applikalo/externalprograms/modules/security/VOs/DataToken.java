package com.seratic.applikalo.externalprograms.modules.security.VOs;


public class DataToken {

    String prefijo;
    String idPublico;
    Integer versionAplicacion;
    String urlApiWrite;
    String urlApiRead;

    public DataToken() {
    }

    public DataToken(String prefijo, String idPublico, Integer versionAplicacion, String urlApiWrite, String urlApiRead) {
        this.prefijo = prefijo;
        this.idPublico = idPublico;
        this.versionAplicacion = versionAplicacion;
        this.urlApiWrite = urlApiWrite;
        this.urlApiRead = urlApiRead;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getIdPublico() {
        return idPublico;
    }

    public void setIdPublico(String idPublico) {
        this.idPublico = idPublico;
    }

    public Integer getVersionAplicacion() {
        return versionAplicacion;
    }

    public void setVersionAplicacion(Integer versionAplicacion) {
        this.versionAplicacion = versionAplicacion;
    }

    public String getUrlApiWrite() {
        return urlApiWrite;
    }

    public void setUrlApiWrite(String urlApiWrite) {
        this.urlApiWrite = urlApiWrite;
    }

    public String getUrlApiRead() {
        return urlApiRead;
    }

    public void setUrlApiRead(String urlApiRead) {
        this.urlApiRead = urlApiRead;
    }
}

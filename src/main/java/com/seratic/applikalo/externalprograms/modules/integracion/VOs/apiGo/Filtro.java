package com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo;

import java.util.ArrayList;
import java.util.List;

public class Filtro {
    private List<Where> filtros;
    private List<CampoOrden> orden;

    public Filtro() {
        this.filtros = new ArrayList<>();
    }

    public List<Where> getFiltros() {
        return filtros;
    }

    public void setFiltros(List<Where> filtros) {
        this.filtros = filtros;
    }

    public List<CampoOrden> getOrden() {
        return orden;
    }

    public void setOrden(List<CampoOrden> orden) {
        this.orden = orden;
    }

    public void addOrden(String campo, boolean ascendente) {
        if (this.orden == null) {
            this.orden = new ArrayList<>();
        }
        CampoOrden campoOrden = new CampoOrden(campo, ascendente);
        this.orden.add(campoOrden);
    }

    public void addFiltro(String objeto, String campo, String condicion , String valor){
        Where where = new Where(objeto,campo,condicion,"'"+ valor +"'");
        this.filtros.add(where);
    }

    public void addFiltroSinComillaSimple(String objeto, String campo,String condicion , String valor){
        Where where = new Where(objeto,campo,condicion,valor);
        this.filtros.add(where);
    }
}

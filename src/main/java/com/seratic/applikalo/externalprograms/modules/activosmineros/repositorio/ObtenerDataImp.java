package com.seratic.applikalo.externalprograms.modules.activosmineros.repositorio;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.activosmineros.dto.ConstanteDTO;
import com.seratic.applikalo.externalprograms.modules.activosmineros.dto.PasivoDTO;
import com.seratic.applikalo.externalprograms.modules.activosmineros.service.ConsultaHttp;
import com.seratic.applikalo.externalprograms.modules.entities.Componente;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo.Filtro;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo.Where;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class ObtenerDataImp implements ObtenerData {

    @Value("${activos-mineros.envio-notificacion.urlData}")
    private String urlEmaData;
    @Value(value = "${activos-mineros.envio-notificacion.id-publico}")
    private String idPublico;
    @Value(value = "${activos-mineros.envio-notificacion.idFuente-proyectos}")
    private String idFuenteProyetcos;
    @Value(value = "${activos-mineros.envio-notificacion.idFuente-pasivos}")
    private Integer idFuentePasivo;
    @Value(value = "${activos-mineros.envio-notificacion.idFuente-verificacion-proyecto}")
    private Integer idFuenteVerificacionProyecto;
    @Value(value = "${activos-mineros.envio-notificacion.idversionApp}")
    private Integer idVerssionApp;
    private RestTemplate restTemplate = new RestTemplate();
    private static final Log LOG = LogFactory.getLog(ObtenerDataImp.class);
    @Autowired
    private ObtenerDataApiGo dataApiGo;
    @Autowired
    private ConsultaHttp consultaHttp;
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public List<JsonNode> obtenerVerificacionProyecto() {
        List<JsonNode> retorno;
        String urlVerificacionProyecto = idFuenteVerificacionProyecto + "/" + idVerssionApp + "/" + idPublico;
        JsonNode dataVerificacionProyectos = dataApiGo.getObjetosApiCamposPaginacion(urlVerificacionProyecto, null);
        retorno = Arrays.asList(new ObjectMapper().convertValue(dataVerificacionProyectos, JsonNode[].class).clone());
        return retorno;
    }

    @Override
    public Componente obtenerComponentePorId(Integer idComponente) {
        Componente retorno = new Componente();
        try {
            retorno = consultaHttp.getHttp(urlEmaData + "/componente/findcomponentedtobyidcomponente?idComponente=" + idComponente, Componente.class);
        }catch (Exception e) {
            LOG.error("Fallo consultado componente en Applikalo", e);
        }
        return retorno;
    }

    @Override
    public ObjectNode obtenerProyectoPorKey(Where filtroJson, String idPublicoOwner) {
        ObjectNode retorno = null;
        String urlObjestoProyecto = idFuenteProyetcos + "/" + idVerssionApp + "/" + idPublicoOwner;
        List<Where> filtroProyecto = new ArrayList<>(0);
        filtroProyecto.add(filtroJson);
        Filtro filtro = new Filtro();
        filtro.setFiltros(filtroProyecto);
        JsonNode dataProyecto = dataApiGo.getObjetosApiCamposPaginacion(urlObjestoProyecto, filtro);
        ObjectNode responseProyecto = mapper.convertValue(dataProyecto.get(0), ObjectNode.class);
        List<PasivoDTO> pasivosAll = obtenerPasivos();
        List<PasivoDTO> pasivosProyecto = new ArrayList<>();
        pasivosAll.stream().forEach(pasivo -> {
            if (Objects.nonNull(pasivo.getProyecto()) && Objects.equals(responseProyecto.get("id").asInt(), pasivo.getProyecto().getId())){
                pasivosProyecto.add(pasivo);
            }
            responseProyecto.set("proyecto_pasivos", mapper.valueToTree(pasivosProyecto));
        });
        retorno = responseProyecto;

        return retorno;
    }

    /*
         Se realiza la consulta de proyecto en bruto, luego se hace match con los pasivos relacionados al proyecto
         */
    @Override
    public List<JsonNode> obtenerProyectos() {
        List<JsonNode> retorno = new ArrayList<>();
        String urlProyectos = idFuenteProyetcos + "/" + idVerssionApp + "/" + idPublico;
        JsonNode dataProyectos = dataApiGo.getObjetosApiCamposPaginacion(urlProyectos, null);
        ObjectNode[] responseProyectos = mapper.convertValue(dataProyectos, ObjectNode[].class);
        List<PasivoDTO> pasivosAll = obtenerPasivos();
        Arrays.asList(responseProyectos).stream().forEach(proyecto -> {
            List<PasivoDTO> pasivosProyecto = new ArrayList<>();
            pasivosAll.stream().forEach(pasivo -> {
                if (Objects.nonNull(pasivo.getProyecto()) && Objects.equals(proyecto.get("id").asInt(), pasivo.getProyecto().getId())){
                    pasivosProyecto.add(pasivo);
                }
            });
            proyecto.putPOJO("proyecto_pasivos", pasivosProyecto);
            retorno.add(proyecto);
        });
        return Arrays.asList(mapper.convertValue(retorno, JsonNode[].class).clone());
    }

    @Override
    public List<PasivoDTO> obtenerPasivos() {
        List<PasivoDTO> retorno;
        String urlPasivos = idFuentePasivo + "/" + idVerssionApp + "/" + idPublico;
        JsonNode respose = dataApiGo.getObjetosApiCamposPaginacion(urlPasivos, null);
        retorno = Arrays.asList(new ObjectMapper().convertValue(respose, PasivoDTO[].class).clone());
        return retorno;
    }

    @Override
    public Componente obtenerComponenteNode(String nombre, Integer versionAplicacion) {
        Componente retorno = new Componente();
        try {
            Map<String, String> params = new HashMap<>(0);
            params.put("nombre", nombre);
            params.put("versionAplicacion", String.valueOf(versionAplicacion));
            retorno = consultaHttp.getHttp(urlEmaData + "componente/nombre/" + nombre + "/versionApp/" + versionAplicacion, Componente.class);
        }catch (Exception e) {
            LOG.error("Fallo consultado componenteNodo en Applikalo", e);
        }
        return retorno;
    }

    @Override
    public ConstanteDTO obtenerConstantePorLlaveEmaData(String llave) {
        return restTemplate.getForObject(urlEmaData, ConstanteDTO.class, llave);
    }

    @Override
    public Boolean actualizarAtributosComponente(ObjectNode obj) {
        Boolean retorno = false;
        try {
            retorno = consultaHttp.postHttp(urlEmaData + "/componente/actualizarAtributoComponente", Boolean.class, obj);
        } catch (Exception e) {
            LOG.error(" Fallo actualizando el componenteNodo en Appliñao ", e);
        }
        return retorno;
    }
}

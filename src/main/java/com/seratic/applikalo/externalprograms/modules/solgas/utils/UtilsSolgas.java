package com.seratic.applikalo.externalprograms.modules.solgas.utils;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.objectmanager.dto.CampoDTO;
import com.seratic.applikalo.externalprograms.objectmanager.dto.FiltroDTO;
import com.seratic.applikalo.externalprograms.objectmanager.dto.ObjectManagerGetRequestDTO;

public class UtilsSolgas {

    private ObjectMapper mapper = new ObjectMapper();

    public ObjectManagerGetRequestDTO getBodyMedidorByEstate(Integer idEstadoMedidor) {
        ObjectManagerGetRequestDTO body = new ObjectManagerGetRequestDTO();
        List<FiltroDTO> filtros = new ArrayList<>(0);
        filtros.add(new FiltroDTO("id", "=", "estado_lectura_medidor", String.valueOf(idEstadoMedidor)));
        List<CampoDTO> campos = new ArrayList<>(0);
        campos.add(new CampoDTO("id"));
        campos.add(new CampoDTO("medidor.centro"));
        campos.add(new CampoDTO("medidor.codigo"));
        campos.add(new CampoDTO("fecha_creacion"));
        campos.add(new CampoDTO("medidor.ultimalectura"));
        campos.add(new CampoDTO("medidor.presionfabrica"));
        body.setObjeto("lectura_medidor");
        body.setFiltros(filtros);
        body.setCampos(campos);
        return body;
    }

    public String bodyUpdateEstadoMedidor(JsonNode medidor, Integer idEstsdoMeidorUpdate) {
        JsonNode returnState = mapper.createObjectNode();
        ((ObjectNode) returnState).put("id", medidor.get("id").asInt());
        JsonNode updateState = mapper.createObjectNode();
        ((ObjectNode) updateState).put("id", idEstsdoMeidorUpdate);
        ((ObjectNode) returnState).putPOJO("estado_lectura_medidor", updateState);
        return "{" + "lectura_medidor:" + returnState + "}";
    }
    
}

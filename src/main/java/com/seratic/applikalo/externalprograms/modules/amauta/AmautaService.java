package com.seratic.applikalo.externalprograms.modules.amauta;

import com.seratic.applikalo.externalprograms.modules.amauta.VOs.AsignarLeccionesResponse;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;

public interface AmautaService {
    AsignarLeccionesResponse asignarLecciones(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
    RespuestaVO calcularAvanceCurso(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
    RespuestaVO calcularAvanceCursoFeedBack(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
    RespuestaVO asignarEvaluacionTareas(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
    boolean asignarFeedAdminCron();
    boolean aprobarFeedAdminCron();
    boolean notificarCorreoEstudianteLeccionCron();
    RespuestaVO actualizarEstadoEstudianteLeccion(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
    RespuestaVO calcularAvanceRespuestaFeedBack(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
}

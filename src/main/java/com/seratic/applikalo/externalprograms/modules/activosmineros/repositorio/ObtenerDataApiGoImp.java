package com.seratic.applikalo.externalprograms.modules.activosmineros.repositorio;

import com.fasterxml.jackson.databind.JsonNode;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo.Filtro;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;

import static com.seratic.applikalo.externalprograms.modules.activosmineros.utils.ConstantesLlave.KEYAUTHORIZATION;
import static com.seratic.applikalo.externalprograms.modules.activosmineros.utils.ConstantesLlave.KEYUSERAGENT;

@Repository
public class ObtenerDataApiGoImp implements ObtenerDataApiGo {

    @Value(value = "${activos-mineros.envio-notificacion.urlReader}")
    private String urlApiRead;
    @Value(value = "${activos-mineros.envio-notificacion.schema}")
    private String schema;
    @Value(value = "${activos-mineros.envio-notificacion.idversionApp}")
    private String idVerssionApp;
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public JsonNode getObjetosApiCamposPaginacion(String url, Filtro filtros) {
        JsonNode data = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(KEYAUTHORIZATION, schema + idVerssionApp);
        headers.add(KEYUSERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<Filtro> request = new HttpEntity<>(filtros, headers);
        JsonNode response = restTemplate.exchange(urlApiRead + "/funcion/campospaginacion/" + url, HttpMethod.POST, request, JsonNode.class).getBody();
        if (Objects.nonNull(response) && response.isObject()) {
            data = response.get("data");
        }
        return data;
    }
}

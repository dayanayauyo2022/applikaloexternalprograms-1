package com.seratic.applikalo.externalprograms.modules.amauta.VOs;

import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyError;

import java.util.List;

public class EstudianteCursoVO {
    String identificador;
    boolean estado;
    String mensaje;
    String codigoError;
    List<LeccionVO> estudiante_leccionLst;
    List<MyError> errores;

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(String codigoError) {
        this.codigoError = codigoError;
    }

    public List<LeccionVO> getEstudiante_leccionLst() {
        return estudiante_leccionLst;
    }

    public void setEstudiante_leccionLst(List<LeccionVO> estudiante_leccionLst) {
        this.estudiante_leccionLst = estudiante_leccionLst;
    }

    public List<MyError> getErrores() {
        return errores;
    }

    public void setErrores(List<MyError> errores) {
        this.errores = errores;
    }
}

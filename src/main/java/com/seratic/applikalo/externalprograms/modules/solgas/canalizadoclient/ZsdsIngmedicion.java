//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.25 a las 10:32:01 AM COT 
//


package com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para ZsdsIngmedicion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ZsdsIngmedicion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Centro" type="{urn:sap-com:document:sap:soap:functions:mc-style}char4"/&gt;
 *         &lt;element name="Medidor" type="{urn:sap-com:document:sap:rfc:functions}char18"/&gt;
 *         &lt;element name="FechaLectura" type="{urn:sap-com:document:sap:soap:functions:mc-style}date10"/&gt;
 *         &lt;element name="HoraLectura" type="{urn:sap-com:document:sap:soap:functions:mc-style}time"/&gt;
 *         &lt;element name="Lectura" type="{urn:sap-com:document:sap:soap:functions:mc-style}quantum10.3"/&gt;
 *         &lt;element name="Presion" type="{urn:sap-com:document:sap:soap:functions:mc-style}quantum10.3"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZsdsIngmedicion", propOrder = {
    "centro",
    "medidor",
    "fechaLectura",
    "horaLectura",
    "lectura",
    "presion"
})
public class ZsdsIngmedicion {

    @XmlElement(name = "Centro", required = true)
    protected String centro;
    @XmlElement(name = "Medidor", required = true)
    protected String medidor;
    @XmlElement(name = "FechaLectura", required = true)
    protected String fechaLectura;
    @XmlElement(name = "HoraLectura", required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar horaLectura;
    @XmlElement(name = "Lectura", required = true)
    protected BigDecimal lectura;
    @XmlElement(name = "Presion", required = true)
    protected BigDecimal presion;

    /**
     * Obtiene el valor de la propiedad centro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentro() {
        return centro;
    }

    /**
     * Define el valor de la propiedad centro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentro(String value) {
        this.centro = value;
    }

    /**
     * Obtiene el valor de la propiedad medidor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedidor() {
        return medidor;
    }

    /**
     * Define el valor de la propiedad medidor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedidor(String value) {
        this.medidor = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaLectura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaLectura() {
        return fechaLectura;
    }

    /**
     * Define el valor de la propiedad fechaLectura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaLectura(String value) {
        this.fechaLectura = value;
    }

    /**
     * Obtiene el valor de la propiedad horaLectura.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHoraLectura() {
        return horaLectura;
    }

    /**
     * Define el valor de la propiedad horaLectura.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHoraLectura(XMLGregorianCalendar value) {
        this.horaLectura = value;
    }

    /**
     * Obtiene el valor de la propiedad lectura.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLectura() {
        return lectura;
    }

    /**
     * Define el valor de la propiedad lectura.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLectura(BigDecimal value) {
        this.lectura = value;
    }

    /**
     * Obtiene el valor de la propiedad presion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPresion() {
        return presion;
    }

    /**
     * Define el valor de la propiedad presion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPresion(BigDecimal value) {
        this.presion = value;
    }

}

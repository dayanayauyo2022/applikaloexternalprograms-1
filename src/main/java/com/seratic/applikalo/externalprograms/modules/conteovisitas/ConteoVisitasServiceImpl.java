package com.seratic.applikalo.externalprograms.modules.conteovisitas;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.IntegracionRepository;
import com.seratic.applikalo.externalprograms.modules.integracion.IntegrationService;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity.ObjetoConteo;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.service.ObjetoConteoService;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.conteoactividades.utils.ConteoActividadesUtils.generarPkObjetoConteo;
import static com.seratic.applikalo.externalprograms.modules.conteovisitas.utils.UtilsConteoVisitas.getBodyConteoVisitas;
import static com.seratic.applikalo.externalprograms.modules.conteovisitas.utils.UtilsConteoVisitas.getBodyUpdateUsuarioConteo;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.FechasUtils.formatDateConSinTimeZoneSave;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.getAmbiente;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.verificarElementoEnListado;
import static com.seratic.applikalo.externalprograms.modules.security.utils.TokenManagerUtil.generarTokenGetObjeto;

@Service
public class ConteoVisitasServiceImpl implements ConteoVisitasService {
    private static final Logger logger = LoggerFactory.getLogger(ConteoVisitasServiceImpl.class);

    @Autowired
    IntegrationService integrationService;

    @Autowired
    private ObjetoConteoService objetoConteoService;

    @Autowired
    private IntegracionRepository integracionRepository;

    @Override
    public RespuestaVO contarVisitas(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoVisita = objetoRequestJson.get("objeto");
            String nombreObjetoVisita = objetoRequestJson.get("nombreObjeto").asText();
            String ultimoNodo = objetoRequestJson.get("ultimoNodo").asText();
            String idObjetoVisita = objetoVisita.get("id").asText();
            String nombreObjetoUsuario = "usuario";
            String nombreContador = "visitas";
            String nombreCampoHoraVisita = "hora_primera_visita";
            boolean mantenerSinTimeZone = true;
            if (objetoRequestJson.get("mantenerSinTimeZone") != null) {
                mantenerSinTimeZone = objetoRequestJson.get("mantenerSinTimeZone").asBoolean();
            }
            String campoContadorDia = nombreContador + "_dia";
            String campoContadorMes = nombreContador + "_mes";

            DataToken dataToken = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
            String tokenEncript = generarTokenGetObjeto(dataToken);

            // buscar visita con su relacion a usuario:
            String bodyVisita = getBodyConteoVisitas(idObjetoVisita, campoContadorDia, campoContadorMes);
            JsonNode visitaConUsuario = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyVisita, nombreObjetoVisita).get(0);
            if (visitaConUsuario != null && visitaConUsuario.get(nombreObjetoUsuario) != null) {
                JsonNode miUsuario = visitaConUsuario.get(nombreObjetoUsuario);
                String idUsuario = miUsuario.get("id").asText();

                // consultar registro de usuario en mongodb para actualizarlo o crear un nuevo registro en caso de que no exista.
                String ambienteObj = getAmbiente(urlRead);
                String pkObjetoConteo = generarPkObjetoConteo(ambienteObj, prefijo, versionApp, nombreObjetoUsuario, idUsuario);
                ObjetoConteo objetoConteo = objetoConteoService.getObjetoConteoById(pkObjetoConteo);
                List<String> contadores = new ArrayList<>();
                if (objetoConteo == null) {
                    objetoConteo = new ObjetoConteo();
                    objetoConteo.setId(pkObjetoConteo);
                    objetoConteo.setIdObjetoApi(idUsuario);
                    objetoConteo.setNombre(nombreObjetoUsuario);
                    objetoConteo.setPrefijo(prefijo);
                    objetoConteo.setVersion(versionApp);
                    objetoConteo.setUrlApiRead(urlRead);
                    objetoConteo.setUrlApiWrite(urlWrite);
                } else {
                    if (objetoConteo.getContadores() != null) {
                        contadores.addAll(objetoConteo.getContadores());
                    }
                }
                objetoConteo.setIdPublicoUsuario(idPublico);

                if (!verificarElementoEnListado(nombreContador, contadores)) {
                    contadores.add(nombreContador);
                }

                Integer valorContadorDia = 1;
                Integer valorContadorMes = 1;
                try {
                    if (miUsuario.get(campoContadorDia) != null && !miUsuario.get(campoContadorDia).isNull()) {
                        valorContadorDia += miUsuario.get(campoContadorDia).asInt();
                        valorContadorMes += miUsuario.get(campoContadorMes).asInt();
                    }
                } catch (Exception e) {
                }

                // actualizar objeto usurio en el api go:
                ((ObjectNode) miUsuario).put(campoContadorDia, valorContadorDia);
                ((ObjectNode) miUsuario).put(campoContadorMes, valorContadorMes);

                // si es la primera visita del dia, poner la hora:
                if (valorContadorDia == 1) {
                    String fechaHoraVisita = formatDateConSinTimeZoneSave(new Date(), mantenerSinTimeZone);
                    ((ObjectNode) miUsuario).put(nombreCampoHoraVisita, fechaHoraVisita);
                }
                if (miUsuario.get("id_publico") != null) {
                    ((ObjectNode) miUsuario).remove("id_publico");
                }

                String bodyUpdateObject = getBodyUpdateUsuarioConteo(miUsuario);
                integrationService.actualizarObjetoApi(dataToken, bodyUpdateObject, nombreObjetoUsuario, true);

                // guardar o actualizar objeto conteo en mongodb
                objetoConteo.setContadores(contadores);
                objetoConteoService.saveUpdateObjetoConteo(objetoConteo);
            } else {
                errList.addError("1.0", "Error visita", "verifique que exista la visita y su relación con usurio. idVisita: " + idObjetoVisita);
                throw errList;
            }
        } catch (MyErrorListException e) {
            if (e.getErrors() != null) {
                errList.addErrorList(e.getErrors());
            }
            if (errList.getErrors() == null || (errList.getErrors() != null && errList.getErrors().size() == 0)) {
                errList.addError("412", "Error conteo de contarVisitas", e.getMessage());
            }
            logger.info("error al contar contarVisitas: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } catch (Exception e) {
            errList.addError("412", "Error conteo de contarVisitas", e.getMessage());
            logger.info("Error conteo de contarVisitas: " + e.getMessage());
        }

        if (errList.getErrors().size() > 0) {
            logger.info("error aconteo de actividades: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } else {
            logger.info("****EXITO. CONTEO DE VISITAS ***");
            RespuestaVO response = new RespuestaVO();
            response.setCode("200");
            response.setMessage("success");
            return response;
        }
    }

}

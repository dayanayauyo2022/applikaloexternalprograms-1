package com.seratic.applikalo.externalprograms.modules.amauta;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.security.TokenManager;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class LeccionListener {

    @Autowired
    AmautaService amautaService;

    // TOPIC Local
    @KafkaListener(topics = "${kafka.amauta.t1}", groupId = "groupid")
    public void asignarLeccionesAmauta(String message) {
        asignarLecciones(message);
    }

    // TOPIC DESA Y BETA comentar esta seccion en desarrollo y descomentar la parte local
    @KafkaListener(topics = {"${kafka.amauta.t2}"}, groupId = "groupid")
    public void asignarLeccionesAmautaTdp(String message) {
        asignarLecciones(message);
    }
    
    private void asignarLecciones(String mensaje){
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode messageObject = mapper.readTree(mensaje);
            String token = messageObject.get("token").asText();
            DataToken dataToken = TokenManager.getDataSourceInfo(token);
            if (dataToken != null) {
                String prefijo = dataToken.getPrefijo();
                Integer versionAplicacion =  dataToken.getVersionAplicacion();
                String idPublico =  dataToken.getIdPublico();
                String urlApiWrite = dataToken.getUrlApiWrite();
                String urlApiRead = dataToken.getUrlApiRead();
                amautaService.asignarLecciones(mensaje, prefijo, versionAplicacion,idPublico, urlApiWrite, urlApiRead);
            }
        } catch (MyErrorListException myEx) {
            myEx.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

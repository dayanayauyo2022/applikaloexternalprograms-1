package com.seratic.applikalo.externalprograms.modules.solgas.service;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 *
 * @author MONDRAGON
 */
public interface SolgasService {

    void insertMediciones(String shcema, String idPublico, String versionApplicacion, String urlApiWrite, String urlApiRead, Integer idEstadoMedidor) throws JsonProcessingException;
    void insertCorteReconexiones() throws JsonProcessingException;
    boolean calcularPromedio(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);

    void programacionAutomatica();
}

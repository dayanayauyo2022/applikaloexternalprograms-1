package com.seratic.applikalo.externalprograms.modules.integracion;

import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/integracion")
public class IntegracionController {
    private static final Logger logger = LoggerFactory.getLogger(IntegracionController.class);

    @Autowired
    IntegrationService integrationService;

    @RequestMapping(method = GET, value = "/ejecutarCronProgExternos")
    @ResponseBody
    public ResponseEntity<?> ejecutarCronProgExternos() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    integrationService.ejecutarProgExternosErroneos(),
                    headers,
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    private String serializeErrors(Object object){
        return new JSONSerializer()
                .exclude("*.class")
                .exclude("cause")
                .exclude("localizedMessage")
                .exclude("stackTraceDepth")
                .serialize(object);
    }
}

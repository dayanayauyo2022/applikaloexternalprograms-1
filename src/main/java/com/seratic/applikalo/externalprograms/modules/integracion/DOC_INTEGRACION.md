## /integracion/ejecutarCronProgExternos

Es una tarea automática que se ejecuta cada diez minutos todos los días. El proceso consiste en consultar la colección 
"prog_externo_error" de la bd mongo para obtener todos los programas externos que tuvieron algun error en su ejecución. 
Luego, con esta lista se procede a lanzar nuevamente cada programa externo, teniendo en cuenta que cada vez que se 
vuelve a ejecutar un programa externo se aumenta un contador y se valida que este contador no pase de 100, cuando llega
a ese valor, se elimina de la bd de mongo para no volverlo a ejecutar en el futuro.

**Nota**: la tabla prog_externo_error de la bd mongo se va actualizando cuando ocurre un error en la ejecución del 
programa externo [Asignar Lecciones](src/main/java/com/seratic/programasExternos/modules/amauta/AMAUTA.md#/amauta/asignarLecciones)

Para ejecutar este proceso en cualquier momento se dispone de un servicio GET:

* URL: http://ema2progextbeta.latinapps.co/integracion/ejecutarCronProgExternos


package com.seratic.applikalo.externalprograms.modules.amauta.VOs;

public class AsignarLeccionesResponse {
    EstudianteCursoVO estudianteCurso;

    public EstudianteCursoVO getEstudianteCurso() {
        return estudianteCurso;
    }

    public void setEstudianteCurso(EstudianteCursoVO estudianteCurso) {
        this.estudianteCurso = estudianteCurso;
    }
}

package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.funciones;

import com.fasterxml.jackson.databind.JsonNode;

public interface FuncionesService {
    String ejecutarFuncionJavascript(String script, JsonNode listaObjetoWf, String nomObjetoBase);
}

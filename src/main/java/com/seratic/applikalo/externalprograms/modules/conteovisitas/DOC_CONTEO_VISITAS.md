## /visitas/conteoVisitas

Servicio POST para la ejecución del programa de contar visitas. El objeto que se espera en la petición es una
visita. El proceso que se realiza es consutar el usuario relacionado con el objeto visita e incrementar
los contadores visitas_dia y visitas_mes, en caso de ser la primera visita se debe poner la fecha y hora
actual en el campo hora_primera_visita.

Para reiniciar los contadores cada día se utiliza el mismo método del conteo de actividade:
[Cron Reiniciar Contadores](src/main/java/com/seratic/programasExternos/modules/amauta/AMAUTA.md#/amauta/notificarCorreoEstudianteLeccionCron)

**Nota:** para el funcionamiento correcto de este programa, el objeto usuario debe tener los siguientes 
campos registrados en el api go:
- visitas_dia
- visitas_mes
- hora_primera_visita

### Ejemplo ###

* URL: http://ema2progextbeta.latinapps.co/visitas/conteoVisitas
* Headers: ``` token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmVmaWpvIjoiZGVzYSIsInZlcnNpb25BcGxpY2FjaW9uIjo3MjEsImlkUHVibGljbyI6IjNmYmViODEzZWM1MmYwZjBlYTFhOTJlNmIzMzAyMjM2N2NmNjdhZDRhOWJjNzgwZTZhYzUwYzYwOGY5MDM2OWUiLCJ1cmxBcGlXcml0ZSI6Imh0dHBzOi8vZW1hMndyaXRlcmRlc2EubGF0aW5hcHBzLmNvL2FwaXdyaXRlLyIsInVybEFwaVJlYWQiOiJodHRwczovL2VtYTJyZWFkZXJkZXNhLmxhdGluYXBwcy5jby9hcGlyZWFkLyJ9.EMtqJOWjx02FPmyJ8aMb1_NRKvrQtOoHrb_S3nC9Y8A
* Body:
    ```
        {
          "objeto": {
              "id": 12
          },
          "nombreObjeto": "visita",
          "ultimoNodo": "Nodo inicial"
        }
    ```




package com.seratic.applikalo.externalprograms.modules.activosmineros.service;

public interface ConsultaHttp {

    <T> T getHttp(String url, Class<T> classResponse);
    <T> T postHttp(String url, Class<T> classResponse, Object body);
}

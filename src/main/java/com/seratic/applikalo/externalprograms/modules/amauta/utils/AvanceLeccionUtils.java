package com.seratic.applikalo.externalprograms.modules.amauta.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;

import java.util.ArrayList;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.amauta.ConstantsAmauta.*;

public class AvanceLeccionUtils {

    public static String getBodyEstudLecciones(String idEstudianteLeccion) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("objeto", "estudiante_leccion");
            ((ObjectNode) filtro1).put("parametro", idEstudianteLeccion);
            filtros.add(filtro1);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante_leccion");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoIdEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudiante).put("campo", "estudiante.id");
            campos.add(campoIdEstudiante);

            JsonNode campoIdCurso = mapper.createObjectNode();
            ((ObjectNode) campoIdCurso).put("campo", "leccion.modulo.curso.id");
            campos.add(campoIdCurso);

            JsonNode campoTpLeccion = mapper.createObjectNode();
            ((ObjectNode) campoTpLeccion).put("campo", "leccion.tipo_leccion.nombre");
            campos.add(campoTpLeccion);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante_leccion getBodyEstudLecciones", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyEstudiantesLecciones(String idEstudiante, String idCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "activo");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "estudiante_leccion");
            ((ObjectNode) filtro1).put("parametro", "'true'");
            filtros.add(filtro1);

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "id");
            ((ObjectNode) filtro2).put("condicional", "=");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", "estudiante_leccion.estudiante");
            ((ObjectNode) filtro2).put("parametro", idEstudiante);
            filtros.add(filtro2);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("conjuncion", "AND");
            ((ObjectNode) filtro3).put("objeto", "estudiante_leccion.leccion.modulo.curso");
            ((ObjectNode) filtro3).put("parametro", idCurso);
            filtros.add(filtro3);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 500000);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante_leccion");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoActivo = mapper.createObjectNode();
            ((ObjectNode) campoActivo).put("campo", "activo");
            campos.add(campoActivo);

            JsonNode campoProcesado = mapper.createObjectNode();
            ((ObjectNode) campoProcesado).put("campo", "procesado");
            campos.add(campoProcesado);

            JsonNode campoIdEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudiante).put("campo", "estudiante.id");
            campos.add(campoIdEstudiante);

            JsonNode campoIdCurso = mapper.createObjectNode();
            ((ObjectNode) campoIdCurso).put("campo", "leccion.modulo.curso.id");
            campos.add(campoIdCurso);

            JsonNode campoTipoLeccion = mapper.createObjectNode();
            ((ObjectNode) campoTipoLeccion).put("campo", "leccion.tipo_leccion.nombre");
            campos.add(campoTipoLeccion);
            
            JsonNode campoEstadoLeccion = mapper.createObjectNode();
            ((ObjectNode) campoEstadoLeccion).put("campo", "estado_estudiante_leccion.nombre");
            campos.add(campoEstadoLeccion);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante_leccion getEstudiantesLecciones", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyEstudianteCurso(String idEstudiante, String idCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "activo");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "estudiante_curso");
            ((ObjectNode) filtro1).put("parametro", "'true'");
            filtros.add(filtro1);

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "id");
            ((ObjectNode) filtro2).put("condicional", "=");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", "estudiante_curso.estudiante");
            ((ObjectNode) filtro2).put("parametro", idEstudiante);
            filtros.add(filtro2);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("conjuncion", "AND");
            ((ObjectNode) filtro3).put("objeto", "estudiante_curso.curso");
            ((ObjectNode) filtro3).put("parametro", idCurso);
            filtros.add(filtro3);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante_curso");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoActivo = mapper.createObjectNode();
            ((ObjectNode) campoActivo).put("campo", "activo");
            campos.add(campoActivo);

            JsonNode campoAvanceLeccion = mapper.createObjectNode();
            ((ObjectNode) campoAvanceLeccion).put("campo", "avance_leccion");
            campos.add(campoAvanceLeccion);

            JsonNode campoAvanceFeed = mapper.createObjectNode();
            ((ObjectNode) campoAvanceFeed).put("campo", "avance_feed");
            campos.add(campoAvanceFeed);

            JsonNode campoAvanceRefeed = mapper.createObjectNode();
            ((ObjectNode) campoAvanceRefeed).put("campo", "avance_refeed");
            campos.add(campoAvanceRefeed);

            JsonNode campoIdEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudiante).put("campo", "estudiante.id");
            campos.add(campoIdEstudiante);

            JsonNode campoIdCurso = mapper.createObjectNode();
            ((ObjectNode) campoIdCurso).put("campo", "curso.id");
            campos.add(campoIdCurso);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante_curso getBodyEstudianteCurso", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyActualizarAvance(String idEstudianteCurso, String avanceTotal, Double valorAvance, String tipoAvance) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).put("id", idEstudianteCurso);
            ((ObjectNode) camposObj).put("avancetotal", avanceTotal);
            if (tipoAvance.equals(TIPO_AVANCE_LECCION)) {
                ((ObjectNode) camposObj).put("avance_leccion", valorAvance);
            } else if (tipoAvance.equals(TIPO_AVANCE_FEEDBACK)) {
                ((ObjectNode) camposObj).put("avance_feed", valorAvance);
            } else if (tipoAvance.equals(TIPO_AVANCE_REFEED)) {
                ((ObjectNode) camposObj).put("avance_refeed", valorAvance);
            }

            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put("estudiante_curso", camposObj);
            return objUpdte.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error getBodyActualizarAvance generando body update estudiante_curso " + idEstudianteCurso, "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyUpdateEstudianteLeccion(String idEstudianteLeccion) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).put("id", idEstudianteLeccion);
            ((ObjectNode) camposObj).put("procesado", true);

            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put("estudiante_leccion", camposObj);
            return objUpdte.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error getBodyUpdateEstudianteLeccion generando body update estudiante_leccion " + idEstudianteLeccion, "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyUpdateRefeed(String idRefeed) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).put("id", idRefeed);
            ((ObjectNode) camposObj).put("avance_calculado", true);

            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put("feedback1", camposObj);
            return objUpdte.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error getBodyActualizarAvance generando body update feedback1 " + idRefeed, "ex: " + e.getMessage());
            throw errList;
        }
    }
}

package com.seratic.applikalo.externalprograms.modules.integracion;

public class Constants {
    public static final String SECRET_KEY_API_READ = "S3R4TIC_4PIR34D";                                                      
    public static final String SECRET_KEY_PROG_EXTERNOS = "S3R4TIC_4PIR34D";

    public static final String URL_API_READ_DESA = "https://ema2readerdesa.latinapps.co/apiread/";
    public static final String URL_API_READ_ED = "https://ema2edgoreader.latinapps.co/apiread/";
    public static final String URL_API_READ_SERATIC = "https://ema2seraticreader.latinapps.co/apiread/";
    public static final String URL_API_READ_RELEASE = "https://ema2regoreader.latinapps.co/apiread/";
    public static final String URL_API_READ_TDP = "https://ema2readertdp.latinapps.co/apiread/";
    public static final String URL_API_READ_BETA = "https://ema2releasereader.latinapps.co/apiread/";

    public static final String URL_API_WRITE_DESA = "https://ema2writerdesa.latinapps.co/apiwrite/";
    public static final String URL_API_WRITE_ED = "https://ema2edgowriter.latinapps.co/apiwrite/";
    public static final String URL_API_WRITE_SERATIC = "https://ema2writerseratic.latinapps.co/apiwrite/";
    public static final String URL_API_WRITE_RELEASE = "https://ema2regowriter.latinapps.co/apiwrite/";
    public static final String URL_API_WRITE_TDP = "https://ema2writertdp.latinapps.co/apiwrite/";
    public static final String URL_API_WRITE_BETA = "https://ema2releasewriter.latinapps.co/apiwrite/";

    public static final String URL_API_EMAREPORTES_DESA = "https://ema2reportesdesa.latinapps.co/";
    public static final String URL_API_EMAREPORTES_ED = "https://ema2reportesed.latinapps.co/";
    public static final String URL_API_EMAREPORTES_SERATIC = "https://ema2reportesseratic.latinapps.co/";
    public static final String URL_API_EMAREPORTES_TDP = "https://ema2reporteswftdp.latinapps.co/";
    public static final String URL_API_EMAREPORTES_BETA = "https://ema2releasereportes.latinapps.co/";

    public static final String AMBIENTE_DESA = "DESA";
    public static final String AMBIENTE_ED = "ED";
    public static final String AMBIENTE_SERATIC = "SERATIC";
    public static final String AMBIENTE_RELEASE = "RELEASE";
    public static final String AMBIENTE_TDP = "TDP";
    public static final String AMBIENTE_BETA = "REBETA";

    public static final String TIPO_PETICION_POST = "POST";
    public static final String TIPO_PETICION_GET = "GET";
    public static final Integer NUM_EJECUCIONES_PROG_EXTERNOS_ERRONEOS = 100;

    // timezone
    public static final String GMT_TIPO = "GMT";

    // estados wf
    public static final Integer WF_ESTADO_MANUAL = 100;
}

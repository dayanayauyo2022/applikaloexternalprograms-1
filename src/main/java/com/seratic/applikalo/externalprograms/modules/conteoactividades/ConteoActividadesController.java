package com.seratic.applikalo.externalprograms.modules.conteoactividades;

import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/actividades")
public class ConteoActividadesController {
    private static final Logger logger = LoggerFactory.getLogger(ConteoActividadesController.class);

    @Autowired
    ConteoActividadesService conteoActividadesService;

    @RequestMapping(method = POST, value = "/conteoActividades", produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public ResponseEntity<?> conteoActividades(@RequestBody String requestBody,
                                               @RequestAttribute("prefijo") String prefijo,
                                               @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                               @RequestAttribute("idPublico") String idPublico,
                                               @RequestAttribute("urlApiWrite") String urlApiWrite,
                                               @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    conteoActividadesService.contarActividades(requestBody, prefijo, versionAplicacion,
                            idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            logger.info("CONTROLLER : " + serializeErrors(myEx.getErrors()));
            return new ResponseEntity<>(serializeErrors(myEx.getErrors()), headers, HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    @RequestMapping(method = POST, value = "/conteoActividadesSinRangoEstados", produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public ResponseEntity<?> conteoActividadesSinRangoEstados(@RequestBody String requestBody,
                                               @RequestAttribute("prefijo") String prefijo,
                                               @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                               @RequestAttribute("idPublico") String idPublico,
                                               @RequestAttribute("urlApiWrite") String urlApiWrite,
                                               @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    conteoActividadesService.contarActividadesSinRagonEstados(requestBody, prefijo, versionAplicacion,
                            idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            logger.info("CONTROLLER : " + serializeErrors(myEx.getErrors()));
            return new ResponseEntity<>(serializeErrors(myEx.getErrors()), headers, HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    @RequestMapping(method = POST, value = "/conteoActividadesSinRangoEstadosBeta", produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public ResponseEntity<?> conteoActividadesSinRangoEstadosBeta(@RequestBody String requestBody,
                                                              @RequestAttribute("prefijo") String prefijo,
                                                              @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                                              @RequestAttribute("idPublico") String idPublico,
                                                              @RequestAttribute("urlApiWrite") String urlApiWrite,
                                                              @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    conteoActividadesService.contarActividadesSinRagonEstadosBeta(requestBody, prefijo, versionAplicacion,
                            idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            logger.info("CONTROLLER : " + serializeErrors(myEx.getErrors()));
            return new ResponseEntity<>(serializeErrors(myEx.getErrors()), headers, HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    @RequestMapping(method = GET, value = "/reiniciarContadores")
    @ResponseBody
    public ResponseEntity<?> reiniciarContadores() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            conteoActividadesService.reiniciarContadores();
            return new ResponseEntity<>(
                    "ok",
                    headers,
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    private String serializeErrors(Object object){
        return new JSONSerializer()
                .exclude("*.class")
                .exclude("cause")
                .exclude("localizedMessage")
                .exclude("stackTraceDepth")
                .serialize(object);
    }
}

package com.seratic.applikalo.externalprograms.modules.entities;

public class TipoAccion extends Tipo{
    private TipoComponenteObj tipoComponenteSiguiente = null;
    private TipoComponenteObj tipoComponentePadre;
    private boolean hasConfigPerfiles;

    public TipoAccion() {
        // default implementation ignored
    }

    public TipoComponenteObj getTipoComponenteSiguiente() {
        return tipoComponenteSiguiente;
    }

    public void setTipoComponenteSiguiente(TipoComponenteObj tipoComponenteSiguiente) {
        this.tipoComponenteSiguiente = tipoComponenteSiguiente;
    }

    public TipoComponenteObj getTipoComponentePadre() {
        return tipoComponentePadre;
    }

    public void setTipoComponentePadre(TipoComponenteObj tipoComponentePadre) {
        this.tipoComponentePadre = tipoComponentePadre;
    }

    public boolean isHasConfigPerfiles() {
        return hasConfigPerfiles;
    }

    public void setHasConfigPerfiles(boolean hasConfigPerfiles) {
        this.hasConfigPerfiles = hasConfigPerfiles;
    }
}

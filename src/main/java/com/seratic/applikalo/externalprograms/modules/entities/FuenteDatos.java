package com.seratic.applikalo.externalprograms.modules.entities;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "objeto",
        "servicio",
        "servicioColumnas",
        "filtrable",
        "nombreObjetoRelacion",
        "nombre",
        "filtro"
})
public class FuenteDatos {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("objeto")
    private Object objeto;
    @JsonProperty("servicio")
    private String servicio;
    @JsonProperty("servicioColumnas")
    private Object servicioColumnas;
    @JsonProperty("filtrable")
    private Boolean filtrable;
    @JsonProperty("nombreObjetoRelacion")
    private Object nombreObjetoRelacion;
    @JsonProperty("nombre")
    private String nombre;
    @JsonProperty("filtro")
    private Object filtro;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("servicio")
    public String getServicio() {
        return servicio;
    }

    @JsonProperty("servicio")
    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    @JsonProperty("servicioColumnas")
    public Object getServicioColumnas() {
        return servicioColumnas;
    }

    @JsonProperty("servicioColumnas")
    public void setServicioColumnas(Object servicioColumnas) {
        this.servicioColumnas = servicioColumnas;
    }

    @JsonProperty("filtrable")
    public Boolean getFiltrable() {
        return filtrable;
    }

    @JsonProperty("filtrable")
    public void setFiltrable(Boolean filtrable) {
        this.filtrable = filtrable;
    }

    @JsonProperty("nombreObjetoRelacion")
    public Object getNombreObjetoRelacion() {
        return nombreObjetoRelacion;
    }

    @JsonProperty("nombreObjetoRelacion")
    public void setNombreObjetoRelacion(Object nombreObjetoRelacion) {
        this.nombreObjetoRelacion = nombreObjetoRelacion;
    }

    @JsonProperty("nombre")
    public String getNombre() {
        return nombre;
    }

    @JsonProperty("nombre")
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @JsonProperty("filtro")
    public Object getFiltro() {
        return filtro;
    }

    @JsonProperty("filtro")
    public void setFiltro(Object filtro) {
        this.filtro = filtro;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
package com.seratic.applikalo.externalprograms.modules.conteoactividades;

import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;

public interface ConteoActividadesService {
    RespuestaVO contarActividades(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
    RespuestaVO contarActividadesSinRagonEstados(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
    RespuestaVO contarActividadesSinRagonEstadosBeta(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
    void reiniciarContadores();
}

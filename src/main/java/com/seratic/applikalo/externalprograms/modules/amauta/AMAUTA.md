## /amauta/asignarLecciones

Servicio POST. Programa externo para la asignación de lecciones a los estudiantes. 
Se toma el registro estudiante_curso relacionado al wf, luego se busca las lecciones correspondintes al curso
y se realiza la insercion de la relacion estudiante-leccion en la tabla estudiante_leccion.
El body del servicio es el mismo que se envia en los programas externos.

### Ejemplo ###

* URL: http://ema2progextbeta.latinapps.co/amauta/asignarLecciones
  * Headers: ``` token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmVmaWpvIjoiZGVzYSIsInZlcnNpb25BcGxpY2FjaW9uIjo3OTUsImlkUHVibGljbyI6IjNmYmViODEzZWM1MmYwZjBlYTFhOTJlNmIzMzAyMjM2N2NmNjdhZDRhOWJjNzgwZTZhYzUwYzYwOGY5MDM2OWUiLCJ1cmxBcGlXcml0ZSI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC8iLCJ1cmxBcGlSZWFkIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgxLyJ9.Jxn1ZK9B7eoS5FHTK0WSFQTp7IDGp_PuQQp2W9_qHwA```
  * Body:
  ```
  {
      "objeto": {
          "id": 3
      },
      "nombreObjeto": "estudiante_curso",
      "ultimoNodo": "Nodo inicial"
  }          
    ```
 
* RESPUESTA:
 - status 200: cuando todo el proceso es exitoso
 - status 400: cuando no pudo realizar ninguna acción. en la respuesta se especifica el subcodigo de errores
 la respusta contiene un campo principal:
  - estudianteCurso: es el registro de estudiante_curso que fue procesado. Tiene sus campos como identificador 
    que es el id de la tabla registro_estudiante, tiene un estado, si esta en true
    significa que el proceso de ese registro fue exitoso de lo contrario aparece en false y lo errores se guardan en el
    campo errores, mensaje y codigoError. cuando es exitoso, en el campo estudiante_leccionLst se muestran los registros
    de estudiante_leccion que fueron insertados (el campo identificador es el id de la tabla estudiante_leccion)
 ```
    ejemplo status 200:
        {
            "estudianteCurso": {
                "identificador": "5",
                "estado": true,
                "mensaje": null,
                "codigoError": null,
                "estudiante_leccionLst": [
                    {
                        "identificador": "212"
                    },
                    {
                        "identificador": "213"
                    }
                ],
                "errores": null
            }
        }
  ```
  ```
ejemplo status 400:
{
    "estudianteCurso": {
        "identificador": "3",
        "estado": false,
        "mensaje": "no procesado",
        "codigoError": "400",
        "estudiante_leccionLst": null,
        "errores": [
            {
                "code": "10.1",
                "answer": "Error obteniendo lista estudiante_leccion api",
                "message": "ex: I/O error on POST request for \"http://localhost:8081/funcion/getObjeto\": Connection refused: connect; nested exception is java.net.ConnectException: Connection refused: connect"
            }
        ]
    }
} 
```

## /amauta/calcularAvanceCurso

Servicio POST para calcular el avance de las lecciones de un estudiante. El registro estudiante_leccion
que llega en el body debe tener el campo procesado en false de lo contrario no se realiza ninguna acción.
Cuando el campo procesado esta en false, se buscan las demas lecciones del estudiante en el curso
determinado y se realiza el calculo del avance de la siguiente manera:
avance = (numProcesados * 90) / total.
numProcesados: numero de registros estudiante_leccion con procesado = true, incluyendo el registro actual
total: total de registros estudiante_leccion para el estudiante en el curso determinado.

El body mantiene la estructura usada para los programas externos:
- objeto: son los campos del objeto relacionado con el wf
- nombreObjeto: es el nombre del objeto relacionado cn el wf
- ultimoNodo: es el nodo actual que ejecuto el programa externo

### Ejemplo ###

* URL: http://ema2progextbeta.latinapps.co/amauta/calcularAvanceCurso
* Headers: ``` token : eyJhbGciOiJIUzI1NiJ9.eyJpZFB1YmxpY28iOiIzZmJlYjgxM2VjNTJmMGYwZWExYTkyZTZiMzMwMjIzNjdjZjY3YWQ0YTliYzc4MGU2YWM1MGM2MDhmOTAzNjllIiwidmVyc2lvbkFwbGljYWNpb24iOjc5NSwicHJlZmlqbyI6ImRlc2EiLCJ1cmxBcGlSZWFkIjoiaHR0cHM6Ly9lbWEycmVhZGVyZGVzYS5sYXRpbmFwcHMuY28vYXBpcmVhZC8iLCJ1cmxBcGlXcml0ZSI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC8ifQ.MZQAUHWuNvmv4wTLHjIrgirIP3pEGvLyf82egPt8rUQ
* Body:
    ```
        {
            "objeto": {
                "activo": true,
                "procesado": false,
                "id": "447"
            },
            "nombreObjeto": "estudiante_leccion",
            "ultimoNodo": "Finalizado"
        }
    ```
  
## /amauta/calcularAvanceCursoFeedback
 
 Servicio POST para calcular el avance de las lecciones feedback de un estudiante. El avance de lecciones en la tabla
 feedback_estudiante corresponde al 10% del total de avance de lecciones para cada estudiante, el cual se almacena en
 la tabla estudiante_curso. El calculo se realiza de la siguiente manera:
 avanceFeedback = (totalAvanceFeedback * 10) / total.
 totalAvanceFeedback: es el total de registros feedback_estudiante en un estado diferente al incial para un estudiante
 total: es el numero total de lecciones tipo evaluación escrita 
 
 El body mantiene la estructura usada para los programas externos:
 - objeto: son los campos del objeto relacionado con el wf
 - nombreObjeto: es el nombre del objeto relacionado cn el wf
 - ultimoNodo: es el nodo actual que ejecuto el programa externo
 
 ### Ejemplo ###
 
 * URL: http://ema2progextbeta.latinapps.co/amauta/calcularAvanceCursoFeedback
 * Headers: ``` token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmVmaWpvIjoiZGVzYSIsInZlcnNpb25BcGxpY2FjaW9uIjo3OTUsImlkUHVibGljbyI6IjNmYmViODEzZWM1MmYwZjBlYTFhOTJlNmIzMzAyMjM2N2NmNjdhZDRhOWJjNzgwZTZhYzUwYzYwOGY5MDM2OWUiLCJ1cmxBcGlXcml0ZSI6Imh0dHBzOi8vZW1hMndyaXRlcmRlc2EubGF0aW5hcHBzLmNvL2FwaXdyaXRlLyIsInVybEFwaVJlYWQiOiJodHRwczovL2VtYTJyZWFkZXJkZXNhLmxhdGluYXBwcy5jby9hcGlyZWFkLyJ9.c9EHDH2xfaArgIQT8fZe0C2szgFdjkBfsW3BEpKHfUg
 * Body:
     ```
         {
             "objeto": {
                 "id": "447"
             },
             "nombreObjeto": "feedback_estudiante",
             "ultimoNodo": ""
         }
     ```
   
## /amauta/asignarEvaluacionTareas(feedback)
 
 Servicio POST para asignar lecciones a evaluar a los estudiantes de un mismo curso y grado. Se buscan los estudiantes
 que cumplen las condiciones:
 - categoria: puede ser 1A, 1B Y 1C. Los estudiantes de 1A pueden ser evaluados por los de 1A o el admin,
 los de 1B pueden ser evaluados por los de 1A y 1B, los de 1C pueden ser evaluados por 1A, 1B, 1C.
 - grado: deben pertenecer al mismo grado. Ejemplo: primaria o secundaria.
 - el numero máximo de estudiantes a asignar debe ser de 5, si hay menos de esa cantidad se selecciona al admin
 como evaluador.

 cambios realizados al metodo segun ticket EMAGO-1075
 
 El body mantiene la estructura usada para los programas externos:
 - objeto: son los campos del objeto relacionado con el wf
 - nombreObjeto: es el nombre del objeto relacionado cn el wf
 - ultimoNodo: es el nodo actual que ejecuto el programa externo
 
 ### Ejemplo ###
 
 * URL: http://ema2progextbeta.latinapps.co/amauta/asignarEvaluacionTareas
 * Headers: ``` token : eyJhbGciOiJIUzI1NiJ9.eyJpZFB1YmxpY28iOiJlMGRkMTU5MjI5MzRkYjQwYmVlNDVjZTk0ZThmNGQ4NzFiZDJhMWM5MTY4MWFlY2Y1NjI0NDQ1ZTNkNzhiZWMwIiwidmVyc2lvbkFwbGljYWNpb24iOjc5NSwicHJlZmlqbyI6ImRlc2EiLCJ1cmxBcGlSZWFkIjoiaHR0cDovL2xvY2FsaG9zdDo4MDkwLyIsImV4cCI6MTc0MjE0MTc0OCwidXJsQXBpV3JpdGUiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvIn0.OMw78Wdw5QCORId-s1OKssXouT6AbNkMJJULhy41GDk
 * Body:
     ```
         {
            "objeto": {
                "activo": true,
                "procesado": false,
                "id": "540991"
            },
            "nombreObjeto": "estudiante_leccion",
            "ultimoNodo": "Realizado",
            "idPublicoOwner": "3fbeb813ec52f0f0ea1a92e6b33022367cf67ad4a9bc780e6ac50c608f90369e",
            "mantenerSinTimeZone": true,
            "gmt": "GMT-5"
        }
     ```
   
## /amauta/asignarFeedAdminCron
 
 Tarea automática que se ejecuta cada día a la 1:55 am. El objetivo es verificar los feedback_estudiante que 
 tienen un mes de haber sido creados y aun no han cambiado su estado a Aprobado o Desaprobado por cada 
 estudiante_lección. Luego se verifica que por cada estudiante_leccion los feedback_estudiante esten en estado
 Pendiente o Desaprobados sin tener ningun registro como Aprobado, Además se verifia que no este asigando al admin,
 si cumple esta condición se realiza la asignación de estudiante_leccion al Admin amauta. 
 Ejemplo: si la fecha en que se ejecuta el cron es 18-11-2020 entonces se verifican los registros de feedback_estudiante
 creados el 19-10-2020
 Existe un servicio GET para ejecutar el cron en cualquier momento:
 
 ### Ejemplo ###
 
 * URL: http://ema2progextbeta.latinapps.co/amauta/ejecutarCronAsignarFeedbackAdmin
 
## /amauta/notificarCorreoEstudianteLeccionCron
 
 Tarea automática que se ejecuta cada día a las 8:00 am. El objetivo es enviar correo de notificación a los
 estudiantes matriculados en una conferencia que se va a realizar en los últimos 3 días. 
 Ejemplo: si la fecha de la conferencia (leccion.fecha) es el 4-11-2020 se debe empezar a notificar 
 desde el 2-11-2020 hasta el dia de la conferencia. 
 Existe un servicio GET para ejecutar el cron en cualquier momento:
 
 ### Ejemplo ###
 
 * URL: http://ema2progextbeta.latinapps.co/amauta/ejecutarCronNotificarEstudiantes
 
## /amauta/ActualizarEstadoWFEstudianteLeccion

Servicio POST. Programa externo que permite, desde el wf feedback_estudiante, cambiar el estado del wf de 
estudiante_leccion en el estado "Finalizado". 
El body del servicio es el mismo que se envia en los programas externos.

### Ejemplo ###

* URL: http://ema2progextbeta.latinapps.co/amauta/actualizarEstadoEstudianteLeccion
  * Headers: ``` token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmVmaWpvIjoiZGVzYSIsInZlcnNpb25BcGxpY2FjaW9uIjo3OTUsImlkUHVibGljbyI6IjNmYmViODEzZWM1MmYwZjBlYTFhOTJlNmIzMzAyMjM2N2NmNjdhZDRhOWJjNzgwZTZhYzUwYzYwOGY5MDM2OWUiLCJ1cmxBcGlXcml0ZSI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC8iLCJ1cmxBcGlSZWFkIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgxLyJ9.Jxn1ZK9B7eoS5FHTK0WSFQTp7IDGp_PuQQp2W9_qHwA```
  * Body:
  ```
  {
      "objeto": {
          "id": 3
      },
      "nombreObjeto": "feedback_estudiante",
      "ultimoNodo": "Nodo inicial"
  }          
    ```
## /amauta/calcularAvanceRespuestaFeedback

Servicio POST. Programa externo que permite, desde el wf feedback1, calcular el avance de las respuestas que se dan
al feedback. Se toma los registros de feedback1 que pertenescan al estudiante y curso segun el wf que se esta 
ejecutando y se cuenta un avance por cada lección que relacionada al feedback1. Es decir, si tiene varios feedback1 para
la misma lección, solo se cuenta como un avance por cada lección. Este avance corresponde al 10%, el calculo se realiza 
teniendo en cuenta el total de lecciones tipo evaluación escrita y el total de lecciones relaciondas con feedback1 que ya fueron 
respondidas por el estudiante.
El body del servicio es el mismo que se envia en los programas externos.

### Ejemplo ###

* URL: http://ema2progextbeta.latinapps.co/amauta/calcularAvanceRespuestaFeedback
  * Headers: ``` token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmVmaWpvIjoiZGVzYSIsInZlcnNpb25BcGxpY2FjaW9uIjo3OTUsImlkUHVibGljbyI6IjNmYmViODEzZWM1MmYwZjBlYTFhOTJlNmIzMzAyMjM2N2NmNjdhZDRhOWJjNzgwZTZhYzUwYzYwOGY5MDM2OWUiLCJ1cmxBcGlXcml0ZSI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC8iLCJ1cmxBcGlSZWFkIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgxLyJ9.Jxn1ZK9B7eoS5FHTK0WSFQTp7IDGp_PuQQp2W9_qHwA```
  * Body:
  ```
  {
      "objeto": {
          "id": 3
      },
      "nombreObjeto": "feedback1",
      "ultimoNodo": "Nodo inicial"
  }          
    ```

## /amauta/task/EnvioCorreoTask

Tarea programada que envia correo 8 am a estudiantes que tiene una leccion en vivo a 2, 1 y 0 dias de la fecha de la leccion, se consultan los datos al API Reader
el cual brinda la informacion de los correos de los estudiantes.

 * Body del correo
```
  
    Número de días que faltan (lecciones_en_vivo.fecha -hoy) (2,1,0)
    Fecha de conferencia: (lecciones_en_vivo.fecha)
    Nombre de la conferencia (lecciones_en_vivo.nombre)
    Link de la conferencia  (lecciones_en_vivo.link_conferencia)
            
  ```

  ## /amauta/aprobarFeedAdminCron
 
 Tarea automática que se ejecuta cada día a la 2:55 am. El objetivo es aprobar las lecciones
 de los estudiantes cuyo feedback ha sido asignado al admin,
 y pasados 14 dias aun continue en estado asignado 
 Ejemplo: si un feedback ha sido asignado al admin con fecha del 01/02/2021 y el sistema 
 se encuentra en en el dia 14/02/2021, y este feedback sigue en estado asignado, 
 el sistema automaticamente lo aprobara
 Existe un servicio GET para ejecutar el cron en cualquier momento:
 
 ### Ejemplo ###
 
 * URL: http://ema2progextbeta.latinapps.co/amauta/ejecutarCronAprobarFeedbackAdmin
 
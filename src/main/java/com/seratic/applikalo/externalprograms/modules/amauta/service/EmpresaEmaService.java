package com.seratic.applikalo.externalprograms.modules.amauta.service;

import com.fasterxml.jackson.databind.JsonNode;

public interface EmpresaEmaService {

    JsonNode obtenerEmpresaById();
}

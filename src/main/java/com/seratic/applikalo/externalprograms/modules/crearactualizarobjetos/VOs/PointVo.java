package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs;

public class PointVo {
    
    double lat;
    double lng;
    
    public PointVo() {
        // default implementation ignored
    }

    public PointVo(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}

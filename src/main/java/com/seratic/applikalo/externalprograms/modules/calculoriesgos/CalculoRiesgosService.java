package com.seratic.applikalo.externalprograms.modules.calculoriesgos;

import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;

public interface CalculoRiesgosService {
    RespuestaVO calcularRiesgo(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
}

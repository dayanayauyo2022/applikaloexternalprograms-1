package com.seratic.applikalo.externalprograms.modules.security.utils;

import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import static com.seratic.applikalo.externalprograms.modules.integracion.Constants.SECRET_KEY_API_READ;
import static com.seratic.applikalo.externalprograms.modules.integracion.Constants.SECRET_KEY_PROG_EXTERNOS;

public class TokenManagerUtil {


    /*Método para generar tokens jwt*/
    public final static String generateToken(Map<String, Object> claims, String secretKey) throws UnsupportedEncodingException {
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secretKey.getBytes("UTF-8"))
                .setExpiration(Date.from(Instant.now().plusSeconds(43200L)))
                .compact();
    }

    /*Metodo para obtener token de get objetos en api read*/
    public final static String generarTokenGetObjeto(DataToken dataToken) {
        try {
            Map<String, Object> claims = new HashMap<String, Object>();
            claims.put("prefijo", dataToken.getPrefijo());
            claims.put("versionAplicacion", dataToken.getVersionAplicacion());
            claims.put("idPublico", dataToken.getIdPublico());
            String token = generateToken(claims, SECRET_KEY_API_READ);
            return token;
        } catch (final Exception e) {
            return "";
        }
    }

    /*Metodo para obtener token de programasExternos*/
    public final static String generarTokenProgExternos(DataToken tokenProgExternos) {
        try {
            Map<String, Object> claims = new HashMap<String, Object>();
            claims.put("prefijo", tokenProgExternos.getPrefijo());
            claims.put("versionAplicacion", tokenProgExternos.getVersionAplicacion());
            claims.put("idPublico", tokenProgExternos.getIdPublico());
            claims.put("urlApiWrite", tokenProgExternos.getUrlApiWrite());
            claims.put("urlApiRead", tokenProgExternos.getUrlApiRead());
            String token = generateToken(claims, SECRET_KEY_PROG_EXTERNOS);
            return token;
        } catch (final Exception e) {
            return "";
        }
    }
}

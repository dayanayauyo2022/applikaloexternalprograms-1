package com.seratic.applikalo.externalprograms.modules.amauta.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.List;


@Service
public class ObtenerDataAmautaClaseImp implements ObtenerDataAmautaClaseService {

    @Value(value = "${amauta.enviocorreo-recordatorio.api-read.url}")
    private String urlAPIRead;
    @Value(value = "${amauta.enviocorreo-recordatorio.schema}")
    private String SCHEMA;
    @Value(value = "${amauta.enviocorreo-recordatorio.idversionApp}")
    private Integer IDVERSIONAPP;
    @Value(value = "${amauta.enviocorreo-recordatorio.clases-vivo.idFuente}")
    private Integer IDFUENTECLASESENVIVO;
    @Value(value = "${amauta.enviocorreo-recordatorio.estudiante-curso.idFuente}")
    private Integer IDFUENTEESTUDIANTECURSO;
    @Value(value = "${amauta.enviocorreo-recordatorio.usuarios.idFuente}")
    private Integer IDFUENTEUSUARIOS;
    private RestTemplate restTemplate = new RestTemplate();

    /*
    ** Se realiza la consulta de clases en vivo
    * Se retorna la fecha de cada clase y el curso, ademas los datos de la clase (nombre, link)
     */
    @Override
    public List<Map<String, Object>> obtenerClasesEnVivo() {
        List<Map<String, Object>> retorno = new ArrayList<>();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", SCHEMA + IDVERSIONAPP);
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity request = new HttpEntity(null, headers);
        JsonNode respose = restTemplate.exchange(urlAPIRead + "/funcion/campospaginacion/"  + IDFUENTECLASESENVIVO + "/" + IDVERSIONAPP + "/3fbeb813ec52f0f0ea1a92e6b33022367cf67ad4a9bc780e6ac50c608f90369e", HttpMethod.POST, request, JsonNode.class).getBody();
        if (respose.size() > 0){
            List<Map<String, Object>> data = new ObjectMapper().convertValue(respose.get("data"), List.class);
            if (data.size() > 0){
                data.stream().forEach(claseEnVivo -> {
                    Map<String, Object> dataClase = new HashMap<>(2);
                    dataClase.put("fecha", claseEnVivo.get("fecha").toString());
                    dataClase.put("nombre", claseEnVivo.get("nombre").toString());
                    dataClase.put("link", claseEnVivo.get("link_conferencia").toString());
                    dataClase.put("idCurso", ((Map<String, Object>) claseEnVivo.get("curso")).get("id"));
                    dataClase.put("idPeriodo", ((Map<String, Object>) claseEnVivo.get("periodo")).get("id"));
                    retorno.add(dataClase);
                });
            }
        }
        return retorno;
    }

    /*
     ** Se realiza la consulta de estudiantes curso
     * Se retorna la idCurso de cada estudiante curso y el idUsurio
     */
    @Override
    public List<Map<String, Object>> obtenerEstudianteCurso() {
        List<Map<String, Object>> retorno = new ArrayList<>();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", SCHEMA + IDVERSIONAPP);
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity request = new HttpEntity(null, headers);
        JsonNode respose = restTemplate.exchange(urlAPIRead + "/funcion/campospaginacion/" + IDFUENTEESTUDIANTECURSO + "/" + IDVERSIONAPP + "/3fbeb813ec52f0f0ea1a92e6b33022367cf67ad4a9bc780e6ac50c608f90369e", HttpMethod.POST, request, JsonNode.class).getBody();
        if (respose.size() > 0){
            List<Map<String, Object>> data = new ObjectMapper().convertValue(respose.get("data"), List.class);
            if (data.size() > 0){
                data.stream().forEach(estudianteCurso -> {
                    Map<String, Object> dataEstudianteCurso = new HashMap<>(2);
                    dataEstudianteCurso.put("activo", (boolean) estudianteCurso.get("activo"));
                    dataEstudianteCurso.put("idCurso", ((Map<String, Object>) estudianteCurso.get("curso")).get("id"));
                    dataEstudianteCurso.put("idPeriodo", ((Map<String, Object>) estudianteCurso.get("periodo")).get("id"));
                    dataEstudianteCurso.put("idUsuario", ((Map<String, Object>) ((Map<String, Object>) estudianteCurso.get("estudiante")).get("usuario")).get("id"));
                    retorno.add(dataEstudianteCurso);
                });
            }
        }
        return retorno;
    }

    @Override
    public List<Map<String, Object>> obtenerUsuarios() {
        List<Map<String, Object>> retorno = new ArrayList<>();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", SCHEMA + IDVERSIONAPP);
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity request = new HttpEntity(null, headers);
        JsonNode respose = restTemplate.exchange(urlAPIRead + "/funcion/campospaginacion/" + IDFUENTEUSUARIOS + "/" + IDVERSIONAPP + "/3fbeb813ec52f0f0ea1a92e6b33022367cf67ad4a9bc780e6ac50c608f90369e", HttpMethod.POST, request, JsonNode.class).getBody();
        if (respose.size() > 0){
            List<Map<String, Object>> data = new ObjectMapper().convertValue(respose.get("data"), List.class);
            if (data.size() > 0){
                data.stream().forEach(usuario -> {
                    Map<String, Object> dataEstudianteCurso = new HashMap<>(2);
                    dataEstudianteCurso.put("id", usuario.get("id"));
                    dataEstudianteCurso.put("estado", usuario.get("estado"));
                    dataEstudianteCurso.put("correo", usuario.get("correo"));
                    retorno.add(dataEstudianteCurso);
                });
            }
        }
        return retorno;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ivang
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimDTO {

    @JsonProperty(value = "codigo")
    private String icc;

    public SimDTO() {
    }

    public SimDTO(String icc) {
        this.icc = icc;
    }

    public String getIcc() {
        return icc;
    }

    public void setIcc(String icc) {
        this.icc = icc;
    }
}

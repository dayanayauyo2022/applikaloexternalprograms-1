package com.seratic.applikalo.externalprograms.modules.activosmineros;

import com.fasterxml.jackson.databind.JsonNode;
import com.seratic.applikalo.externalprograms.modules.activosmineros.service.ArmarNotificacionImp;
import com.seratic.applikalo.externalprograms.modules.activosmineros.repositorio.ObtenerData;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo.Where;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/activosmineros")
public class ActivosMinerosController {

    @Autowired
    private ObtenerData obtenerData;
    @Autowired
    private ArmarNotificacionImp armarNotificacion;

    /*
        Este servicio notifica si el Proyecto que se esta creado
        tiene campos vacios, de ser asi ejecuta un nodo notificacion
     */
    @PostMapping(value = "/notificar")
    @ResponseBody
    public ResponseEntity<JsonNode> notificarProtectosYPasivos(@RequestBody(required = false) JsonNode requestBody,
                                                     @RequestAttribute(value = "prefijo", required = false) String prefijo,
                                                     @RequestAttribute(value = "versionAplicacion", required = false) Integer versionAplicacion,
                                                     @RequestAttribute(value = "idPublico", required = false) String idPublico,
                                                     @RequestAttribute(value = "urlApiWrite", required = false) String urlApiWrite,
                                                     @RequestAttribute(value = "urlApiRead", required = false) String urlApiRead){
        try {
            Where filtroPoryecto = new Where(requestBody.get("nombreObjeto").asText(), "key", "=","'" + requestBody.get("objeto").get("key").asText() + "'");
            armarNotificacion.armarNotificacionProyectoCamposVacios(filtroPoryecto, idPublico, prefijo);
            return new ResponseEntity<>(requestBody, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestBody, HttpStatus.EXPECTATION_FAILED);
        }
    }
}

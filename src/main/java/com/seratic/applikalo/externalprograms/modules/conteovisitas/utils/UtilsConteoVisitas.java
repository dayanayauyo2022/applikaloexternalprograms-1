package com.seratic.applikalo.externalprograms.modules.conteovisitas.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;

import java.util.ArrayList;
import java.util.List;

public class UtilsConteoVisitas {

    public static String getBodyConteoVisitas(String idVisita, String campoContadorDia, String campoContadorMes) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "visita");
            ((ObjectNode) filtro1).put("parametro", idVisita);
            filtros.add(filtro1);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", "visita");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoUsername = mapper.createObjectNode();
            ((ObjectNode) campoUsername).put("campo", "usuario.username");
            campos.add(campoUsername);

            JsonNode campoNombresU = mapper.createObjectNode();
            ((ObjectNode) campoNombresU).put("campo", "usuario.nombres");
            campos.add(campoNombresU);

            JsonNode campoEstadoU = mapper.createObjectNode();
            ((ObjectNode) campoEstadoU).put("campo", "usuario.estado");
            campos.add(campoEstadoU);

            JsonNode campoVisitasDia = mapper.createObjectNode();
            ((ObjectNode) campoVisitasDia).put("campo", "usuario." + campoContadorDia);
            campos.add(campoVisitasDia);

            JsonNode campoVisitasMes = mapper.createObjectNode();
            ((ObjectNode) campoVisitasMes).put("campo", "usuario." + campoContadorMes);
            campos.add(campoVisitasMes);

            JsonNode campoPerfil = mapper.createObjectNode();
            ((ObjectNode) campoPerfil).put("campo", "usuario.perfil.id");
            campos.add(campoPerfil);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body visita", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyUpdateUsuarioConteo(JsonNode camposUsuario) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put("usuario", camposUsuario);
            return objUpdte.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error getBodyConteoVisitas " + camposUsuario.get("id").asText(), "ex: " + e.getMessage());
            throw errList;
        }
    }
}

package com.seratic.applikalo.externalprograms.modules.calculoriesgos.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.calculoriesgos.utils.ConstantsRiesgos.TABLA_CHEQUEO_SINTOMAS;
import static com.seratic.applikalo.externalprograms.modules.calculoriesgos.utils.ConstantsRiesgos.getListaCamposRiesgos;

public class CalculoRiesgosUtils {
    public static final String armarObjRiesgosGetObjetoApi(String nombreObjeto, String idObjeto) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro = mapper.createObjectNode();
            ((ObjectNode) filtro).put("campo", "id");
            ((ObjectNode) filtro).put("condicional", "=");
            ((ObjectNode) filtro).put("objeto", nombreObjeto);
            ((ObjectNode) filtro).put("parametro", idObjeto);
            filtros.add(filtro);

            List<JsonNode> camposOrdenArray = new ArrayList<>();
            JsonNode campoOrdenFecha = mapper.createObjectNode();
            ((ObjectNode) campoOrdenFecha).put("campo", "fecha_creacion");
            ((ObjectNode) campoOrdenFecha).put("ascendente", false);
            camposOrdenArray.add(campoOrdenFecha);

            ((ObjectNode) objetoPeticion).putArray("orden").addAll(camposOrdenArray);
            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1000);
            ((ObjectNode) objetoPeticion).put("objeto", "wf_" + nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoNodo = mapper.createObjectNode();
            ((ObjectNode) campoNodo).put("campo", "nodo");
            campos.add(campoNodo);

            JsonNode campoFechaCreacion = mapper.createObjectNode();
            ((ObjectNode) campoFechaCreacion).put("campo", "fecha_creacion");
            campos.add(campoFechaCreacion);

            JsonNode campoObjetoMr = mapper.createObjectNode();
            ((ObjectNode) campoObjetoMr).put("campo", nombreObjeto + ".id");
            campos.add(campoObjetoMr);

            for (String campoChequeo: getListaCamposRiesgos()) {
                JsonNode chequoJso = mapper.createObjectNode();
                ((ObjectNode) chequoJso).put("campo",TABLA_CHEQUEO_SINTOMAS + "." + campoChequeo);
                campos.add(chequoJso);
            }

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);

            return objetoPeticion.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static final boolean verificarValorEnListado(List<String> camposVerificarlst, List<String> lstValoresSintomasRiesgo) {
        boolean estaEnLista = false;
        Collator insenstiveStringComparator = Collator.getInstance();
        insenstiveStringComparator.setStrength(Collator.PRIMARY);
        for (String campoVerificar: camposVerificarlst) {
            for (String valorSintoma: lstValoresSintomasRiesgo) {
                if (insenstiveStringComparator.compare(campoVerificar, valorSintoma) == 0){
                    estaEnLista = true;
                }
            }
        }
        return estaEnLista;
    }

    /*armar peticion para actualizar objeto en api write*/
    public static final String armarObjRiesgoUpdateApiWrite(String nombreObjetoBase, JsonNode camposObjeto, String nivelRiesgo) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objNivelRiesgo = mapper.createObjectNode();
            ((ObjectNode) objNivelRiesgo).put("nivel", nivelRiesgo);

            ((ObjectNode) camposObjeto).put("nivelriesgo", objNivelRiesgo);

            List<JsonNode> listaObjetos = new ArrayList<>();
            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put(nombreObjetoBase, camposObjeto);
            listaObjetos.add(objUpdte);

            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);
            return objToSave.toString();
        } catch (Exception e) {
            return null;
        }
    }
}

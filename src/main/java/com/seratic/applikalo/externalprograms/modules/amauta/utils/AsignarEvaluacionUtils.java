package com.seratic.applikalo.externalprograms.modules.amauta.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;

import java.util.ArrayList;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.amauta.ConstantsAmauta.*;

public class AsignarEvaluacionUtils {

    public static String getNivelesAcademicosAsignar(String nivelAcademicoEstudiante) {
        if (nivelAcademicoEstudiante.equals(NIVEL_1A)) {
            return "'" + NIVEL_1A + "'";
        } else if (nivelAcademicoEstudiante.equals(NIVEL_1B)) {
            return "'" + NIVEL_1A + "','" + NIVEL_1B+ "'";
        } else if (nivelAcademicoEstudiante.equals(NIVEL_1C)) {
            return "'" + NIVEL_1A + "','" + NIVEL_1B+ "','" + NIVEL_1C + "'";
        } else {
            return "";
        }
    }

    public static String getBodyEstudianteLeccionAE(String idEstudianteLeccion) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("conjuncion", "AND");
            ((ObjectNode) filtro3).put("objeto", "estudiante_leccion");
            ((ObjectNode) filtro3).put("parametro", idEstudianteLeccion);
            filtros.add(filtro3);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante_leccion");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoGradoEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoGradoEstudiante).put("campo", "estudiante.grado");
            campos.add(campoGradoEstudiante);

            JsonNode campoNivelEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoNivelEstudiante).put("campo", "estudiante.categoria");
            campos.add(campoNivelEstudiante);

            JsonNode campoNombreTipoLeccion = mapper.createObjectNode();
            ((ObjectNode) campoNombreTipoLeccion).put("campo", "leccion.tipo_leccion.nombre");
            campos.add(campoNombreTipoLeccion);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante_leccion", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyPosiblesEstudiantesAE(String idEstudiante, String nivelesAcademicos, String grado) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "<>");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "estudiante");
            ((ObjectNode) filtro1).put("parametro", idEstudiante);
            filtros.add(filtro1);

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "categoria");
            ((ObjectNode) filtro2).put("condicional", "IN");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", "estudiante");
            ((ObjectNode) filtro2).put("parametro", nivelesAcademicos);
            filtros.add(filtro2);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "grado");
            ((ObjectNode) filtro3).put("condicional", "ILIKE");
            ((ObjectNode) filtro3).put("conjuncion", "AND");
            ((ObjectNode) filtro3).put("objeto", "estudiante");
            ((ObjectNode) filtro3).put("parametro", "'" + grado.trim() + "'");
            filtros.add(filtro3);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("pagesize", 100000);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoGradoEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoGradoEstudiante).put("campo", "grado");
            campos.add(campoGradoEstudiante);

            JsonNode campoNivelEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoNivelEstudiante).put("campo", "categoria");
            campos.add(campoNivelEstudiante);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyAdminAmautaAE() {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "nombre");
            ((ObjectNode) filtro1).put("condicional", "ILIKE");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "usuario.perfil");
            ((ObjectNode) filtro1).put("parametro", "'" + NOMBRE_PERFIL_ADMIN + "'");
            filtros.add(filtro1);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", "usuario");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoNomPerfil = mapper.createObjectNode();
            ((ObjectNode) campoNomPerfil).put("campo", "perfil.nombre");
            campos.add(campoNomPerfil);

            JsonNode campoUsername = mapper.createObjectNode();
            ((ObjectNode) campoUsername).put("campo", "username");
            campos.add(campoUsername);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body usuario", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyFeedbackRegistradosAE(String idLeccion) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "feedback_estudiante.estudiante_leccion.leccion");
            ((ObjectNode) filtro1).put("parametro", idLeccion);
            filtros.add(filtro1);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("pagesize", 100000);
            ((ObjectNode) objetoPeticion).put("objeto", "feedback_estudiante");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoIdLeccion = mapper.createObjectNode();
            ((ObjectNode) campoIdLeccion).put("campo", "estudiante_leccion.leccion.id");
            campos.add(campoIdLeccion);

            JsonNode campoIdEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudiante).put("campo", "estudiante.id");
            campos.add(campoIdEstudiante);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body feedback_estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyInsertFeddbackEstudAE(String idEstudianteLeccion, List<String> idsEstudiantesLst, JsonNode admin, JsonNode evalEscrita,
                                                            String nombreEvalEscrita) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<JsonNode> listaObjetos = new ArrayList<>();

            JsonNode objEstudianteLeccion = mapper.createObjectNode();
            ((ObjectNode) objEstudianteLeccion).put("id", idEstudianteLeccion);
            for (String idEstudiante : idsEstudiantesLst) {
                JsonNode objEstudiante = mapper.createObjectNode();
                ((ObjectNode) objEstudiante).put("id", idEstudiante);

                JsonNode camposObj = mapper.createObjectNode();
                ((ObjectNode) camposObj).set("estudiante_leccion", objEstudianteLeccion);
                ((ObjectNode) camposObj).set("estudiante", objEstudiante);
                ((ObjectNode) camposObj).put("comentariofeed", "");

                if (evalEscrita != null) {
                    ((ObjectNode) camposObj).set(nombreEvalEscrita, evalEscrita);
                }

                JsonNode objInsert = mapper.createObjectNode();
                ((ObjectNode) objInsert).set("feedback_estudiante", camposObj);
                listaObjetos.add(objInsert);
            }
            if (admin != null) {
                JsonNode objUsuario = mapper.createObjectNode();
                ((ObjectNode) objUsuario).put("id", admin.get("id").asText());

                JsonNode camposObj = mapper.createObjectNode();
                ((ObjectNode) camposObj).set("estudiante_leccion", objEstudianteLeccion);
                ((ObjectNode) camposObj).set("usuario", objUsuario);
                ((ObjectNode) camposObj).put("comentariofeed", "");

                if (evalEscrita != null) {
                    ((ObjectNode) camposObj).put(nombreEvalEscrita, evalEscrita);
                }

                JsonNode objInsert = mapper.createObjectNode();
                ((ObjectNode) objInsert).set("feedback_estudiante", camposObj);
                listaObjetos.add(objInsert);
            }

            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);
            return objToSave.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error insertregisterpost generando body insert feedback_estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }
    /*Método para verificar si existe un objeto dentro de un listado json segun la llave y valor*/
    public static final boolean verificarEstudianteEnListaFeed(JsonNode listado, String idEstudiante) {
        boolean existe = false;
        if (listado != null && listado.size() > 0) {
            for (JsonNode itemLista : listado) {
                if (itemLista.get("estudiante") != null && !itemLista.get("estudiante").isNull() && itemLista.get("estudiante").get("id").asText().equals(idEstudiante)) {
                    existe = true;
                }
            }
        }
        return existe;
    }

    public static String getBodyEvalEscrita(String idEstudianteLeccion, String nombreObjeto) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", nombreObjeto + ".estudiante_leccion");
            ((ObjectNode) filtro1).put("parametro", idEstudianteLeccion);
            filtros.add(filtro1);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoEstudLeccion = mapper.createObjectNode();
            ((ObjectNode) campoEstudLeccion).put("campo", "estudiante_leccion.id");
            campos.add(campoEstudLeccion);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body " + nombreObjeto, "ex: " + e.getMessage());
            throw errList;
        }
    }

    /**
     * Metodo para obtener el body de un objeto estudiante_curso en string 
     * 
     * @param filtros filtros que conforman el body
     * @param campos campos solicitados en el body del objecto estudiante_curso 
     * @return String, body con el objeto estudiante_curso segun campos y filtros enviados.
   */
    public static String getBodyEstudianteCurso(List<JsonNode> filtros, List<JsonNode> campos) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            
            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("pagesize", 100000);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante_curso");

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante_leccion", "ex: " + e.getMessage());
            throw errList;
        }
    }

}

package com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo;

public class CampoOrden {
    private String campo;
    private boolean ascendente;

    public CampoOrden() {
    }

    public CampoOrden(String campo, boolean ascendente) {
        this.campo = campo;
        this.ascendente = ascendente;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public boolean isAscendente() {
        return ascendente;
    }

    public void setAscendente(boolean ascendente) {
        this.ascendente = ascendente;
    }
}

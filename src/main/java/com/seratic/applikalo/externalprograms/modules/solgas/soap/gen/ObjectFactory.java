//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.18 a las 03:11:44 PM COT 
//
package com.seratic.applikalo.externalprograms.modules.solgas.soap.gen;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * com.seratic.applikalo.externalprograms.modules.solgas.soap.gen package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of
     * schema derived classes for package:
     * com.seratic.applikalo.externalprograms.modules.solgas.soap.gen
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsertMedidoresRequest }
     *
     */
    public InsertMedidoresRequest createInsertMedidoresRequest() {
        return new InsertMedidoresRequest();
    }

    /**
     * Create an instance of {@link Medidor }
     *
     */
    public Medidor createMedidor() {
        return new Medidor();
    }

    /**
     * Create an instance of {@link InsertTanquesResponse }
     *
     */
    public InsertTanquesResponse createInsertTanquesResponse() {
        return new InsertTanquesResponse();
    }

    /**
     * Create an instance of {@link Tanque }
     *
     */
    public Tanque createTanque() {
        return new Tanque();
    }

    /**
     * Create an instance of {@link InsertEdificiosResponse }
     *
     */
    public InsertEdificiosResponse createInsertEdificiosResponse() {
        return new InsertEdificiosResponse();
    }

    /**
     * Create an instance of {@link Edificio }
     *
     */
    public Edificio createEdificio() {
        return new Edificio();
    }

    /**
     * Create an instance of {@link InsertTanquesRequest }
     *
     */
    public InsertTanquesRequest createInsertTanquesRequest() {
        return new InsertTanquesRequest();
    }

    /**
     * Create an instance of {@link InsertCorteReconexionesRequest }
     *
     */
    public InsertCorteReconexionesRequest createInsertCorteReconexionesRequest() {
        return new InsertCorteReconexionesRequest();
    }

    /**
     * Create an instance of {@link Corteyreconexion }
     *
     */
    public Corteyreconexion createCorteyreconexion() {
        return new Corteyreconexion();
    }

    /**
     * Create an instance of {@link InsertCorteReconexionesResponse }
     *
     */
    public InsertCorteReconexionesResponse createInsertCorteReconexionesResponse() {
        return new InsertCorteReconexionesResponse();
    }

    /**
     * Create an instance of {@link InsertEdificiosRequest }
     *
     */
    public InsertEdificiosRequest createInsertEdificiosRequest() {
        return new InsertEdificiosRequest();
    }

    /**
     * Create an instance of {@link InsertMedidoresResponse }
     *
     */
    public InsertMedidoresResponse createInsertMedidoresResponse() {
        return new InsertMedidoresResponse();
    }

    /**
     * Create an instance of {@link Distrito }
     *
     */
    public Distrito createDistrito() {
        return new Distrito();
    }

    /**
     * Create an instance of {@link Ubicacion }
     *
     */
    public Ubicacion createUbicacion() {
        return new Ubicacion();
    }

    /**
     * Create an instance of {@link Departamento }
     *
     */
    public Departamento createDepartamento() {
        return new Departamento();
    }

    /**
     * Create an instance of {@link Tipos }
     *
     */
    public Tipos createTipos() {
        return new Tipos();
    }

    /**
     * Create an instance of {@link Provincia }
     *
     */
    public Provincia createProvincia() {
        return new Provincia();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.dto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 *
 * @author ivang
 */
@JsonTypeName(value = "instalacion")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class InstalacionDTO extends InstalacionDownDTO {

    public InstalacionDTO(InstalacionDownDTO instalacionDownDTO) {
        id = instalacionDownDTO.id;
        sim = instalacionDownDTO.sim;
        abonado = instalacionDownDTO.abonado;
        contrato = instalacionDownDTO.contrato;
        ubicacion = instalacionDownDTO.ubicacion;
        plataforma = instalacionDownDTO.plataforma;
        enviado = instalacionDownDTO.enviado;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ivang
 */
public class SubscriptionDetailDataDTO {

    @JsonProperty(value = "subscriptionDetailData")
    private SuscriptionDTO suscriptionDTO;

    public SuscriptionDTO getSuscriptionDTO() {
        return suscriptionDTO;
    }

    public void setSuscriptionDTO(SuscriptionDTO suscriptionDTO) {
        this.suscriptionDTO = suscriptionDTO;
    }
}

package com.seratic.applikalo.externalprograms.modules.entities;

public class TipoComponenteObj extends Tipo{

    private String disenioWeb = null;
    private String disenioMovil = null;
    private String atributos;
    private String visualizacion = null;

    public TipoComponenteObj() {
        // default implementation ignored
    }

    public String getDisenioWeb() {
        return disenioWeb;
    }

    public void setDisenioWeb(String disenioWeb) {
        this.disenioWeb = disenioWeb;
    }

    public String getDisenioMovil() {
        return disenioMovil;
    }

    public void setDisenioMovil(String disenioMovil) {
        this.disenioMovil = disenioMovil;
    }

    public String getAtributos() {
        return atributos;
    }

    public void setAtributos(String atributos) {
        this.atributos = atributos;
    }

    public String getVisualizacion() {
        return visualizacion;
    }

    public void setVisualizacion(String visualizacion) {
        this.visualizacion = visualizacion;
    }
}

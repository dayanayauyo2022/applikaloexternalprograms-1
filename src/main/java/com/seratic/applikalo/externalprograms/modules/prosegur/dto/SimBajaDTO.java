/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 *
 * @author ivang
 */
@JsonTypeName(value = "simbaja")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class SimBajaDTO extends SimBajaDownDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String motivo;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("estado_simbaja")
    private EstadoSimBajaDTO estadoSimBaja;

    public SimBajaDTO(String icc) {
        this.icc = icc;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public EstadoSimBajaDTO getEstadoSimBaja() {
        return estadoSimBaja;
    }

    public void setEstadoSimBaja(EstadoSimBajaDTO estadoSimBaja) {
        this.estadoSimBaja = estadoSimBaja;
    }
}

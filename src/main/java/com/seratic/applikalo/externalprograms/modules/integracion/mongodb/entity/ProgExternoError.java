package com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity;

import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyError;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Document(collection = "prog_externo_error")
public class ProgExternoError implements Serializable {
    private static final long serialVersionUID = 2330289880871451995L;
    @Id
    private String id;
    private String url;
    private String tipoPeticion;
    private String body;
    private Integer numeroEjecuciones;
    private String prefijo;
    private Integer versionApp;
    private String idPublicoUsuario;
    private String urlApiWrite;
    private String urlApiRead;
    private List<MyError> erroresLst;
    private Date fecha;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTipoPeticion() {
        return tipoPeticion;
    }

    public void setTipoPeticion(String tipoPeticion) {
        this.tipoPeticion = tipoPeticion;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getNumeroEjecuciones() {
        return numeroEjecuciones;
    }

    public void setNumeroEjecuciones(Integer numeroEjecuciones) {
        this.numeroEjecuciones = numeroEjecuciones;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public Integer getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(Integer versionApp) {
        this.versionApp = versionApp;
    }

    public String getIdPublicoUsuario() {
        return idPublicoUsuario;
    }

    public void setIdPublicoUsuario(String idPublicoUsuario) {
        this.idPublicoUsuario = idPublicoUsuario;
    }

    public String getUrlApiWrite() {
        return urlApiWrite;
    }

    public void setUrlApiWrite(String urlApiWrite) {
        this.urlApiWrite = urlApiWrite;
    }

    public String getUrlApiRead() {
        return urlApiRead;
    }

    public void setUrlApiRead(String urlApiRead) {
        this.urlApiRead = urlApiRead;
    }

    public List<MyError> getErroresLst() {
        return erroresLst;
    }

    public void setErroresLst(List<MyError> erroresLst) {
        this.erroresLst = erroresLst;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}

package com.seratic.applikalo.externalprograms.modules.activosmineros.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PasivoDTO {

    private Integer id;
    private String nrocomp;
    private String fechaplazopasivo;
    private EstadoObj estadopasivo;
    private ProyectoDTO proyecto;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public PasivoDTO() {
        // default implementation ignored
    }

    public Integer getId() {
        return id;
    }

    public String getNrocomp() {
        return nrocomp;
    }

    public void setNrocomp(String nrocomp) {
        this.nrocomp = nrocomp;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFechaplazopasivo() {
        return fechaplazopasivo;
    }

    public void setFechaplazopasivo(String fechaplazopasivo) {
        this.fechaplazopasivo = fechaplazopasivo;
    }

    public EstadoObj getEstadopasivo() {
        return estadopasivo;
    }

    public void setEstadopasivo(EstadoObj estadopasivo) {
        this.estadopasivo = estadopasivo;
    }

    public ProyectoDTO getProyecto() {
        return proyecto;
    }

    public void setProyecto(ProyectoDTO proyecto) {
        this.proyecto = proyecto;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}

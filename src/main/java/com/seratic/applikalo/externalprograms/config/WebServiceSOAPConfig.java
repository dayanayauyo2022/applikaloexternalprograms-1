package com.seratic.applikalo.externalprograms.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceSOAPConfig extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/solgassoap/*");
    }

    @Bean(name = "solgassoap")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema writersoapSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("SolgasSOAPPort");
        wsdl11Definition.setLocationUri("/solgassoap");
        wsdl11Definition.setTargetNamespace("http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen");
        wsdl11Definition.setSchema(writersoapSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema apiwritesoapSchema() {
        return new SimpleXsdSchema(new ClassPathResource("solgassoap.xsd"));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.objectmanager.dto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.util.List;

/**
 *
 * @author ivang
 */
public class ObjectManagerInsertRequest<T> {

    @JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
    private List<T> objetos;

    public ObjectManagerInsertRequest(List<T> objetos) {
        this.objetos = objetos;
    }

    public List<T> getObjetos() {
        return objetos;
    }

    public void setObjetos(List<T> objetos) {
        this.objetos = objetos;
    }
}

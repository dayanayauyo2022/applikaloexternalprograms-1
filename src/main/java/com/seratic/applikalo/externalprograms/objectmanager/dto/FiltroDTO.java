/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.objectmanager.dto;

/**
 *
 * @author ivang
 */
public class FiltroDTO {

    private String campo;
    private String condicional;
    private String objeto;
    private String parametro;

    public FiltroDTO(String campo, String condicional, String objeto, String parametro) {
        this.campo = campo;
        this.condicional = condicional;
        this.objeto = objeto;
        this.parametro = parametro;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getCondicional() {
        return condicional;
    }

    public void setCondicional(String condicional) {
        this.condicional = condicional;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

}

package com.seratic.applikalo.externalprograms;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.search.AndTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.SubjectTerm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.mail.dsl.Mail;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableIntegration
@EnableScheduling
public class ProgramasExternosApplication extends SpringBootServletInitializer {

    @Value("${prosegur.mailDownReceiver.mail.isSSL}")
    private boolean isSSL;
    @Value("${prosegur.mailDownReceiver.mail.user}")
    private String mailUser;
    @Value(value = "${prosegur.mailDownReceiver.mail.userEvent}")
    private String mailUserEvento;
    @Value("${prosegur.mailDownReceiver.mail.password}")
    private String mailPassword;
    @Value(value = "${prosegur.mailDownReceiver.mail.passwordEvent}")
    private String mailPasswordEvento;
    @Value("${prosegur.mailDownReceiver.mail.host}")
    private String mailHost;
    @Value("${prosegur.mailDownReceiver.mail.port}")
    private String mailPort;
    @Value("${prosegur.mailDownReceiver.mail.fixedDelay}")
    private long mailFixedDelay;
    @Value("${prosegur.mailDownReceiver.mail.simDownMessageSubject}")
    private String simDownMessageSubject;
    @Value(value = "${prosegur.mailDownReceiver.mail.eventMessageSubject}")
    private String eventMessageSubject;

    public static void main(String[] args) {
        SpringApplication.run(ProgramasExternosApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ProgramasExternosApplication.class);
    }

    @Bean
    public IntegrationFlow imapMailFlow() {
        return IntegrationFlows.
                from(Mail.
                        imapInboundAdapter((isSSL ? "imaps" : "imap") + "://" + mailUser + ":" + mailPassword + "@" + mailHost + ":" + mailPort + "/INBOX").
                        searchTermStrategy((Flags flags, Folder folder) -> {
                            SubjectTerm subjectTerm = new SubjectTerm(simDownMessageSubject);
                            FlagTerm flagTerm = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
                            return new AndTerm(subjectTerm, flagTerm);
                        }), e -> e.autoStartup(true)).
                channel("imapReceivingChannel").
                get();
    }

    @Bean
    public IntegrationFlow prosegurInsertMailFlow() {
        return IntegrationFlows.
                from(Mail.
                        imapInboundAdapter((isSSL ? "imaps" : "imap") + "://" + mailUserEvento + ":" + mailPasswordEvento + "@" + mailHost + ":" + mailPort + "/INBOX").
                        searchTermStrategy((Flags flags, Folder folder) -> {
                            SubjectTerm subjectTerm = new SubjectTerm(eventMessageSubject);
                            FlagTerm flagTerm = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
                            return new AndTerm(subjectTerm, flagTerm);
                        }).simpleContent(true), e -> e.autoStartup(true)).
                channel("eventReceivingChannel").
                get();
    }

    @Bean(name = PollerMetadata.DEFAULT_POLLER)
    public PollerMetadata defaultPoller() {
        return Pollers.fixedDelay(mailFixedDelay).get();
    }
}
